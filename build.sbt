name := "loreal-pipeline"

version := "0.1"

scalaVersion := "2.11.7"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

resolvers += Resolver.jcenterRepo

resolvers += Resolver.bintrayRepo("unibas-gravis", "maven")

resolvers += "Statismo (private)" at "https://statismo.cs.unibas.ch/repository/private/"


credentials += Credentials(Path.userHome / ".ivy2" / ".credentials-statismo-private")


libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.2"

libraryDependencies += "org.scalaz" %% "scalaz-core" % "7.2.10"


libraryDependencies += "ch.unibas.cs.gravis" %% "faces-scala" % "0.8.3"

libraryDependencies += "ch.unibas.cs.gravis" %% "scalismo-faces" % "0.2.2"

libraryDependencies += "ch.unibas.cs.gravis" %% "scalismo-tools" % "0.4.+"

libraryDependencies += "ch.unibas.cs.gravis" %% "scalismo-ui" % "0.10.+"

libraryDependencies += "com.github.tototoshi" %% "scala-csv" % "1.3.3"

libraryDependencies += "ch.unibas.cs.gravis" %% "ui-plugins" % "0.6.0-RC1"

libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0"

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.7"

libraryDependencies ~= { _.map(_.exclude("org.slf4j", "slf4j-nop")) }

dependencyOverrides += "ch.unibas.cs.gravis" %% "scalismo" % "develop-e956bc6d476133595b3250f6d2f2954ebd10a3cc"


