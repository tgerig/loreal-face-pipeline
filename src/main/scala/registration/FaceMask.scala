package registration

import scalismo.common.{PointId, UnstructuredPointsDomain}
import scalismo.geometry.{Point, _3D}
import scalismo.kernels.GaussianKernel
import scalismo.mesh.{ScalarMeshField, TriangleMesh}
import scalismo.utils.Memoize

case class FaceMask(levelMask: ScalarMeshField[Int], semanticMask: ScalarMeshField[Int]) {


  def isLipPoint(id : PointId) : Boolean = {
    semanticMask(id) == 2
  }

  def isEarRegion(id : PointId) : Boolean = {
    semanticMask(id) == 1
  }

  // Returns a value in the interval [0,1] indicating whether a point belongs to the region
  def computeSmoothedRegions(referenceMesh: TriangleMesh[_3D], level : Int, stddev : Double) : Point[_3D] => Double = {

    val transformedMask = levelMask.copy(mesh = referenceMesh)
    val pointsWithRegions = transformedMask.pointsWithValues.toIndexedSeq

    val regionSmoother = GaussianKernel[_3D](stddev)
    val regionPts = UnstructuredPointsDomain(pointsWithRegions.filter(_._2 >= level).map(_._1))

    def regionWeight(p : Point[_3D]) : Double = {
      regionSmoother(regionPts.findClosestPoint(p).point,p)
    }

    Memoize(regionWeight,referenceMesh.pointSet.numberOfPoints)
  }

}

