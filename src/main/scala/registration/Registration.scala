package registration


import java.awt.Color

import breeze.linalg.{DenseMatrix, DenseVector}
import ch.unibas.cs.gravis.facepipeline.LOrealDataProvider.{Merged, RAW}
import ch.unibas.cs.gravis.facepipeline._
import com.typesafe.scalalogging.LazyLogging
import scalismo.common.{PointId, UnstructuredPointsDomain}
import scalismo.geometry.{Landmark, Point, Vector, _3D}
import scalismo.mesh._
import scalismo.numerics.{LBFGSOptimizer, Sampler, UniformMeshSampler3D}
import scalismo.registration._
import scalismo.statisticalmodel.{DiscreteLowRankGaussianProcess, MultivariateNormalDistribution, StatisticalMeshModel}
import scalismo.utils.{MeshConversion, Random}
import vtk.{vtkDecimatePro, vtkJavaMemoryManager}


/**
  * Created by luetma00 on 20.01.17.
  */
case class Registration(dataProvider: DataProvider) extends PipelineStep with LazyLogging {


  type CoefficientVector = DenseVector[Double]

  case class LandmarkPair(referenceLandmark: Landmark[_3D], targetLandmark: Landmark[_3D])

  case class LevelConfig(regularizationWeight : Double, outlierThreshold : Option[Double], numBasisFunctions : Int, lines : Boolean = true)

  case class OutlierAwarePointSampler(referenceMesh: TriangleMesh[_3D], sampledNumberOfPoints: Int, isValidTargetPoint: Point[_3D] => Boolean)(implicit rand: Random) extends Sampler[_3D] with LazyLogging {

    private val points = UniformMeshSampler3D(referenceMesh, sampledNumberOfPoints).sample()(rand).map(_._1)
    private val validPointsOnly = points.filter(isValidTargetPoint)
    override val numberOfPoints: Int = validPointsOnly.size
    logger.info(s"sampling $numberOfPoints points")
    override def volumeOfSampleRegion: Double = referenceMesh.area

    override def sample()(implicit rand: Random): IndexedSeq[(Point[_3D], Double)] = {
      validPointsOnly.map(p => (p, 1.0 / referenceMesh.area))
    }

  }


  def registration(gpModel: StatisticalMeshModel,
                   targetMesh: TriangleMesh[_3D],
                   faceMask : FaceMask,
                   landmarkPairs: Seq[LandmarkPair],
                   referenceLineLandmarks: Seq[Landmark[_3D]],
                   targetLineLandmarks: Seq[Landmark[_3D]]): TriangleMesh[_3D] = {

    val referenceMesh = gpModel.referenceMesh

    VisualLogger.showTargetMesh(targetMesh)

    val landmarkConstraints = for (landmarkPair <- landmarkPairs.toIndexedSeq) yield {
      val referencePointId = referenceMesh.pointSet.findClosestPoint(landmarkPair.referenceLandmark.point).id
      val targetPoint = landmarkPair.targetLandmark.point
      (referencePointId, targetPoint, landmarkPair.referenceLandmark.uncertainty.get)
    }

    val posteriorModel = gpModel.posterior(landmarkConstraints)

    VisualLogger.ui.map(_.show(VisualLogger.modelGroup.get,referenceLineLandmarks.map(_.point).toIndexedSeq,"Reference Line Landmarks"))
    VisualLogger.ui.map(_.show(VisualLogger.modelGroup.get,landmarkPairs.map(_.referenceLandmark.point).toIndexedSeq,"Reference Landmarks"))
    VisualLogger.ui.map(_.show(VisualLogger.targetGroup.get,targetLineLandmarks.map(_.point).toIndexedSeq,"Target Line Landmarks"))
    VisualLogger.ui.map(_.show(VisualLogger.targetGroup.get,landmarkConstraints.map(_._2).toIndexedSeq,"Target Landmarks"))

    var initialCoefficients = DenseVector.zeros[Double](posteriorModel.rank)

    val levelConfigs = Seq(LevelConfig(1E-1, None, gpModel.rank / 8),
                           LevelConfig(1E-2, None, gpModel.rank / 4),
                           LevelConfig(1E-3, None, gpModel.rank / 2),
                           LevelConfig(1E-4, None, gpModel.rank),
                           LevelConfig(1E-5, None, gpModel.rank),
                           LevelConfig(1E-6, Some(1.0), gpModel.rank)
    )
    val finalCoefficients = levelConfigs.foldLeft[(DenseVector[Double])](initialCoefficients){
      case(currentCoefficients, levelConfig) => {
        registrationForLevel(posteriorModel, targetMesh, faceMask, levelConfig, numberOfIterations = 20,referenceLineLandmarks,targetLineLandmarks,currentCoefficients)
      }
    }


    val transformationSpace = GaussianProcessTransformationSpace[_3D](posteriorModel.gp.interpolateNearestNeighbor)
    val trans = transformationSpace.transformForParameters(finalCoefficients)
    referenceMesh.transform(trans)

  }

  def registrationForLevel(gpModel: StatisticalMeshModel,
                           targetMesh : TriangleMesh[_3D],
                           faceMask: FaceMask,
                           levelConfig : LevelConfig,
                           numberOfIterations: Int,
                           referenceLineLandmarks: Seq[Landmark[_3D]],
                           targetLineLandmarks: Seq[Landmark[_3D]],
                           initialCoefficients: CoefficientVector): CoefficientVector = {

    val LevelConfig(regularizationWeight, outlierThreshold,  numBasisFunctions, lines) = levelConfig

    logger.debug("Start Registration")

    val reducedGPModel = reduceModel(gpModel, numBasisFunctions)
    val reducedInitialCoefficients = initialCoefficients(0 until numBasisFunctions)

    val referenceMesh = reducedGPModel.referenceMesh

    val currentFit = reducedGPModel.instance(reducedInitialCoefficients)

    val transformedLandmarks = referenceLineLandmarks.map(lm => lm.copy(point = currentFit.pointSet.point(referenceMesh.pointSet.findClosestPoint(lm.point).id)))

    val tdLine = (for (transformedLandmark <- transformedLandmarks ) yield {
      val correspondingTargetLm = targetLineLandmarks.minBy((l : Landmark[_3D]) => (l.point - transformedLandmark.point).norm)
      val lmDist = (transformedLandmark.point - correspondingTargetLm.point).norm
      val refId = currentFit.pointSet.findClosestPoint(transformedLandmark.point).id
      (refId, correspondingTargetLm.point, MultivariateNormalDistribution(DenseVector.zeros[Double](3), DenseMatrix.eye[Double](3) * (lmDist)))
    }).toIndexedSeq

    val projectionPoints = UniformMeshSampler3D(referenceMesh, 1000).sample().map(_._1)

    val (gpPost, newInitialParameters) = if (lines){

      val posterior = reducedGPModel.posterior(tdLine)
      val initialDF = reducedGPModel.gp.interpolateNearestNeighbor.instance(reducedInitialCoefficients)
      val newInitialParameters = posterior.gp.interpolateNearestNeighbor.coefficients(projectionPoints.zip(projectionPoints.map(initialDF)),1E-5)

      (posterior, newInitialParameters)

    } else {
      (reducedGPModel, reducedInitialCoefficients)
    }

    VisualLogger.showStatisticalShapeModel(gpPost)
    VisualLogger.updateModelView(newInitialParameters)
    // here we need to compute a new posterior based on the line landmarks


    def isValidTargetPoint(currentFit: TriangleMesh[_3D],
                           targetMeshOps: TriangleMesh3DOperations,
                           targetMeshBoundary: UnstructuredPointsDomain[_3D])
                          (p: Point[_3D]): Boolean = {

      val ptId = referenceMesh.pointSet.findClosestPoint(p).id
      val closestPt = targetMeshOps.closestPointOnSurface(currentFit.pointSet.point(ptId))
      val closestPtId = targetMesh.pointSet.findClosestPoint(closestPt.point).id

      def isOnValidBoundary(ptId : PointId, closestPtId : PointId) : Boolean = {

        if(faceMask.isLipPoint(ptId)) {
          true
        } else {
          (closestPt.point - targetMeshBoundary.findClosestPoint(closestPt.point).point).norm > 0.5
        }

      }

      def getOutlierTreshold(ptId : PointId) : Double = {

        if(faceMask.isLipPoint(ptId)) {
          Double.MaxValue
        } else {
          outlierThreshold.getOrElse(Double.MaxValue)
        }
      }

      Math.sqrt(closestPt.distanceSquared) < getOutlierTreshold(ptId) &&
        isOnValidBoundary(ptId,closestPtId) && !faceMask.isEarRegion(ptId)
    }

    val targetMeshBoundaryPred = MeshBoundaryPredicates(targetMesh)
    val targetMeshBoundary = UnstructuredPointsDomain(targetMesh.pointSet.pointIds
      .filter(targetMeshBoundaryPred.pointIsOnBoundary)
      .map(targetMesh.pointSet.point).toIndexedSeq
    )

    val optimizationPointSampler = OutlierAwarePointSampler(referenceMesh,
      sampledNumberOfPoints = referenceMesh.pointSet.numberOfPoints,
      isValidTargetPoint(currentFit, targetMesh.operations, targetMeshBoundary))

    val config = RegistrationConfiguration[_3D, GaussianProcessTransformationSpace[_3D]](
      optimizer = LBFGSOptimizer(numIterations = numberOfIterations),
      metric = HuberDistanceMetric[_3D](optimizationPointSampler),
      transformationSpace = GaussianProcessTransformationSpace(gpPost.gp.interpolateNearestNeighbor),
      regularizer = L2Regularizer,
      regularizationWeight = regularizationWeight)

    // Scalismo implements registration always as image to image registration.
    // Therefore we compute distance images from the meshes
    val fixedImage = referenceMesh.operations.toDistanceImage
    val movingImage = targetMesh.operations.toDistanceImage

    val registrationIterator = scalismo.registration.Registration.iterations(config)(fixedImage, movingImage, newInitialParameters)
    val iteratorWithLogging = for ((regState, itNum) <- registrationIterator.zipWithIndex) yield {
      logger.debug(s"Iteration $itNum: value = ${regState.optimizerState.value}")
      VisualLogger.updateModelView(regState.optimizerState.parameters)
      regState
    }

    val projectionPointsLargeSet = UniformMeshSampler3D(referenceMesh,2000).sample().map(_._1)
    val lastRegistrationState = iteratorWithLogging.toSeq.last
    val finalDf = gpPost.gp.interpolateNearestNeighbor.instance(lastRegistrationState.optimizerState.parameters)
    val finalParametersOrigGp = gpModel.gp.interpolateNearestNeighbor.coefficients(projectionPointsLargeSet.zip(projectionPointsLargeSet.map(finalDf)), 1e-5)

    (finalParametersOrigGp)

  }


  private def reduceModel(model : StatisticalMeshModel, numBasisFunctions : Int) : StatisticalMeshModel = {
    val reducedGp = DiscreteLowRankGaussianProcess(model.gp.mean, model.gp.klBasis.take(numBasisFunctions))
    model.copy(gp = reducedGp)
  }

  override def run(): Unit = {
    // transforms the mesh using the best similarity transform between the reference and target landmarks.

    for (expression <- LOrealDataProvider.Expressions.expressionList()) {
      val referenceLandmarks = dataProvider.incoming.reference.loadLandmarks(expression).get
      val referenceLineLandmarks = dataProvider.incoming.reference.loadLineLandmarks(expression).get
      val model = dataProvider.registration.loadPriorModel(expression = expression).get

      val faceMask = dataProvider.incoming.reference.loadFaceMask().get

      logger.info("Successfully loaded reference and model")

      for (id <- scala.util.Random.shuffle(dataProvider.incoming.ids(expression)) if dataProvider.registration.loadMesh(id,expression).isFailure &&  dataProvider.incoming.loadLandmarks(id,expression).isSuccess) {

        logger.info("Performing registration for id " + id)

        val targetMesh = dataProvider.incoming.loadMesh(id,expression,flag = Merged,mask = RAW).get
        val targetLandmarks = dataProvider.incoming.loadLandmarks(id,expression).get
        val targetLines = dataProvider.incoming.loadLineLandmarks(id,expression).get

        val correspondingLandmarks = correspondingLandmarkPairs(referenceLandmarks, targetLandmarks)

        val correspondingLandmarkPoints = correspondingLandmarks.map(lmPair => (lmPair.targetLandmark.point, lmPair.referenceLandmark.point))
        val alignmentTransform = LandmarkRegistration.similarity3DLandmarkRegistration(correspondingLandmarkPoints, center = Point(0.0, 0.0, 0.0))
        val alignedTargetMesh = targetMesh.transform(alignmentTransform)
        val alignedLandmarkPairs = correspondingLandmarks.map(lmPair =>
          LandmarkPair(lmPair.referenceLandmark, lmPair.targetLandmark.transform(alignmentTransform))
        )

        val alignedTargetLines = targetLines.map(p => p.transform(alignmentTransform))

        val registeredMesh = registration(model, alignedTargetMesh, faceMask, alignedLandmarkPairs,referenceLineLandmarks,alignedTargetLines)

        // we realign the registered mesh with the target.
        val registeredMeshOrigSpace = registeredMesh.transform(alignmentTransform.inverse)
        dataProvider.registration.saveMesh(id,expression, registeredMeshOrigSpace)
      }
    }
  }

  private def correspondingLandmarkPairs(referenceLandmarks: Seq[Landmark[_3D]], targetLandmarks: Seq[Landmark[_3D]]): Seq[LandmarkPair] = {

    referenceLandmarks
      .map(refLm => (refLm, targetLandmarks.find(targetLm => targetLm.id == refLm.id)))
      .filter(lmTuple => lmTuple._2.nonEmpty)
      .map(lmTuple => LandmarkPair(lmTuple._1, lmTuple._2.get))
  }

}

object Registration {

  def main(args: Array[String]): Unit = {

    scalismo.initialize()
    Registration(LOrealDataProvider).run()

  }
}
