package registration


import breeze.linalg.DenseMatrix
import breeze.stats.distributions.MarkovChain.Kernels
import ch.unibas.cs.gravis.facepipeline.LOrealDataProvider._
import ch.unibas.cs.gravis.facepipeline._
import com.typesafe.scalalogging.StrictLogging
import scalismo.common._
import scalismo.geometry.{Point, Vector, _3D}
import scalismo.io.{MeshIO, StatismoIO}
import scalismo.kernels.{Kernel, BSplineKernel, DiagonalKernel, MatrixValuedPDKernel}
import scalismo.numerics.PivotedCholesky.StoppingCriterion
import scalismo.numerics.{PivotedCholesky, UniformMeshSampler3D}
import scalismo.statisticalmodel._
import scalismo.ui.api.ScalismoUI
import spire.math.UByte

/**
  * Created by luetma00 on 24.01.17.
  */
case class BuildRegistrationPrior(dataProvider: DataProvider) extends PipelineStep with StrictLogging {


  def approximatePointSet(points: UnstructuredPointsDomain[_3D], D: Double, gp : GaussianProcess[_3D,Vector[_3D]], sc: StoppingCriterion) : (DiscreteLowRankGaussianProcess[_3D,Vector[_3D]],UnstructuredPointsDomain[_3D]) = {

    def phiWithDim(i: Int, dim : Int, ptId : Int, phi: DenseMatrix[Double]) = {
      phi(ptId*3 + dim,i)
    }

    def phiVec(i : Int, ptID : PointId,phi : DenseMatrix[Double]) = {
      Vector(phiWithDim(i,0,ptID.id,phi),phiWithDim(i,1,ptID.id,phi),phiWithDim(i,2,ptID.id,phi))
    }

    val (phi,lambda) = PivotedCholesky.computeApproximateEig(gp.cov,points.points.toIndexedSeq,D,sc)

    val nPhi = phi.cols

    val klBasis: DiscreteLowRankGaussianProcess.KLBasis[_3D, Vector[_3D]] = for(i <- 0 until nPhi) yield {
      val v = DiscreteField[_3D,Vector[_3D]](points,points.pointsWithId.toIndexedSeq.map(f => phiVec(i,f._2,phi)))
      DiscreteLowRankGaussianProcess.Eigenpair(lambda(i),v)
    }
    val mean = DiscreteField[_3D,Vector[_3D]](points,points.points.toIndexedSeq.map(p => gp.mean(p)))

    val r = DiscreteLowRankGaussianProcess[_3D,Vector[_3D]](mean, klBasis)
    (r,points)

  }


  override def run(): Unit = {

    scalismo.initialize()

    for (expression <- Seq(Neutral)) yield {
      logger.info(s"building model for expression $expression")
      val referenceMesh = dataProvider.incoming.reference.loadMesh(expression).get
      val mask = dataProvider.incoming.reference.loadFaceMask().get
      val faceKernel = FaceKernel(mask,referenceMesh)
      val gp = GaussianProcess[_3D, Vector[_3D]](faceKernel)
      val ldg = approximatePointSet(referenceMesh.pointSet, 1.0, gp, PivotedCholesky.NumberOfEigenfunctions(2000))
      val lowRankGaussianProcess = ldg._1.interpolateNearestNeighbor
      logger.info("computed nystrom approximation")

      val model = StatisticalMeshModel(referenceMesh, lowRankGaussianProcess)

      dataProvider.registration.savePriorModel(model, expression)

      logger.info("model building done")
    }
  }

}

object BuildRegistrationPrior {

  def main(args: Array[String]): Unit = {
    BuildRegistrationPrior(LOrealDataProvider).run()
  }

}

