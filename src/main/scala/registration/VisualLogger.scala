package registration

import java.awt.Color

import breeze.linalg.DenseVector
import scalismo.geometry._3D
import scalismo.mesh.TriangleMesh
import scalismo.statisticalmodel.StatisticalMeshModel
import scalismo.ui.api._
import scalismo.ui.model.Scene
import scalismo.ui.model.properties.Uncertainty
import scalismo.utils.Random

object VisualLogger {
  var ui : Option[ScalismoUI] = None//Some(ScalismoUI("Visual Logger"))

  val modelGroup = ui.map(_.createGroup("Model"))
  var modelView : Option[StatisticalMeshModelViewControls] = None

  val targetGroup = ui.map(_.createGroup("Target"))
  var targetMeshView : Option[TriangleMeshView] = None



  def showTargetMesh(targetMesh : TriangleMesh[_3D]) : Unit = {
    remove(targetMeshView)
    targetMeshView = show(VisualLogger.targetGroup, targetMesh, "target")
    targetMeshView.map(_.color = Color.RED)
  }

  def showStatisticalShapeModel(ssm : StatisticalMeshModel) : Unit = {
    removeModel(modelView)
    modelView = show(modelGroup, ssm, "gpmodel")
    modelView.map(_.meshView.opacity = 0.7)
  }

  def updateModelView(coeffs : DenseVector[Double]) : Unit = {
    if (modelView.isDefined) {
      modelView.get.shapeModelTransformationView.shapeTransformationView.coefficients = coeffs
    }
  }


  private def show[A](group : Option[Group], t : A, name : String)(implicit sic : ShowInScene[A]): Option[sic.View] = {
    for {
      ui <- ui
      g <- group
    } yield {
      ui.show(g, t, name)
    }
  }

  def remove[V <: ObjectView](view : Option[V]): Unit = {
    view.foreach(_.remove())
  }

  def removeModel(view : Option[StatisticalMeshModelViewControls]): Unit = {
    for {v <- view} {
      v.meshView.remove()
      v.shapeModelTransformationView.remove()
    }
  }

}