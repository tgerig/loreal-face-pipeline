package preprocessing

import ch.unibas.cs.gravis.facepipeline.LOrealDataProvider.{Merged, Original, RAW}
import ch.unibas.cs.gravis.facepipeline.{LOrealDataProvider, DataProvider, PipelineStep}
import com.typesafe.scalalogging.LazyLogging
import registration.Registration
import scalismo.common.{PointId, UnstructuredPointsDomain}
import scalismo.geometry.{Point, _3D}
import scalismo.mesh.{TriangleCell, TriangleList, TriangleMesh, TriangleMesh3D}
import scalismo.utils.MeshConversion
import vtk.vtkDecimatePro

/**
  * Created by gerith00 on 22.02.17.
  */
case class PreprocessShells(dataProvider : DataProvider) extends PipelineStep with LazyLogging {

  def mergeShells(shells : Seq[TriangleMesh[_3D]]) : TriangleMesh[_3D] = {
    val emptyMesh = TriangleMesh3D(UnstructuredPointsDomain[_3D](IndexedSeq[Point[_3D]]()), TriangleList(IndexedSeq[TriangleCell]()))
    shells.foldLeft(emptyMesh)((mergedMesh, shell) => {
      val newDomain = UnstructuredPointsDomain(mergedMesh.pointSet.points.toIndexedSeq ++ shell.pointSet.points.toIndexedSeq)
      val numPointsSoFar = mergedMesh.pointSet.numberOfPoints
      val adjustedCellTriangulation = shell.triangulation.triangles.map(t =>
        TriangleCell(PointId(t.ptId1.id + numPointsSoFar), PointId(t.ptId2.id + numPointsSoFar), PointId(t.ptId3.id + numPointsSoFar)))
      val newTriangulation = TriangleList(mergedMesh.triangulation.triangles ++ adjustedCellTriangulation)
      TriangleMesh3D(newDomain, newTriangulation)
    }
    )
  }

  def reduceMesh(mesh: TriangleMesh[_3D]) : TriangleMesh[_3D] = {
    var meshvtk = MeshConversion.meshToVtkPolyData(mesh)
    var decimator = new vtkDecimatePro()
    val reductionFactor =  math.min(100000.0, mesh.pointSet.numberOfPoints) / mesh.pointSet.numberOfPoints.toDouble
    decimator.SetTargetReduction(1.0 - reductionFactor)
    decimator.SetInputData(meshvtk)
    decimator.Update()
    val reducedMesh = MeshConversion.vtkPolyDataToTriangleMesh(decimator.GetOutput()).get.copy()
    decimator.Delete()
    meshvtk.Delete()
    System.gc()
    reducedMesh
  }


  override def run(): Unit = {

    for (expression <- LOrealDataProvider.Expressions.expressionList()) {

      print(expression)

      for (id <- scala.util.Random.shuffle(dataProvider.incoming.ids(expression)) if !dataProvider.incoming.exists(id, expression,flag = Merged,mask = RAW)) {

        val targetShells = dataProvider.incoming.loadShellMeshes(id,expression)
        val targetMerged = mergeShells(targetShells.map(_.get))
//        val targetMesh = reduceMesh(targetMerged)
        val targetMesh = targetMerged
        dataProvider.incoming.saveMesh(id,expression,flag = Merged, mask = RAW, mesh = targetMesh)

        logger.debug(s"$id created for expression ${expression.toString}")

      }
    }

  }
}

object PreprocessShells {

  def main(args: Array[String]): Unit = {

    scalismo.initialize()
    PreprocessShells(LOrealDataProvider).run()

  }
}
