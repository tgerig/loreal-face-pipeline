package tools

import java.io.File

import breeze.linalg.{DenseMatrix, DenseVector}
import ch.unibas.cs.gravis.facepipeline.LOrealDataProvider
import ch.unibas.cs.gravis.facepipeline.LOrealDataProvider._
import faces.landmarks.{TLMSLandmark3D, TLMSLandmarksIO}
import scalismo.geometry.{Point, _3D}
import scalismo.io.LandmarkIO
import scalismo.statisticalmodel.MultivariateNormalDistribution

import scala.io.Source
import scala.util.Try

/**
  * Created by luetma00 on 03.02.17.
  */
object PrepareReferenceLineLandmarks {

  /** read TLMS 3D format from a stream (format: "id visible x y z") */
  def readTLMS(file: File): Try[IndexedSeq[TLMSLandmark3D]] = Try {
    val lines = Source.fromFile(file).getLines()
    lines.map { line =>
      val fields = line.split("\\s+").map(_.trim)
      val name = fields(0)
      val visibility: Boolean = fields(1).toInt > 0
      val x = fields(2).toFloat
      val y = fields(3).toFloat
      val z = fields(4).toFloat
      TLMSLandmark3D(name, Point(x, y, z), visibility)
    }.toIndexedSeq
  }

  def main(args : Array[String]) : Unit = {

    scalismo.initialize()

    val rawRefLmsFile = new File("/export/tunisia_mnt/gerith00_local/faces/aligned/initial/reference/reference.json")

    // First we load the landmarks of the reference and then set reasonable default uncertainties.

    val refLandmarks = LandmarkIO.readLandmarksJson[_3D](rawRefLmsFile).get

    val referenceLandmarks = for (lm <- refLandmarks) yield {

      val noiseVariance = lm.id.trim match {
        case lmid if lmid.contains("eyebrow") => 3.0
        case lmid if lmid.contains("eye.bottom") => 3.0
        case lmid if lmid.contains("eye.top") => 3.0
        case lmid if lmid.contains("ear") => 3.0
        case _ => 1.0
      }
      lm.copy(uncertainty = Some(MultivariateNormalDistribution(DenseVector.zeros[Double](3), DenseMatrix.eye[Double](3) * noiseVariance)))
    }

    // Transfer the reference landmarks to all the expressions and save them.
    for (expression <- Seq(Neutral)) {

      val neutralRef = LOrealDataProvider.incoming.reference.loadMesh(Neutral).get
      val expressionRef = LOrealDataProvider.incoming.reference.loadMesh(expression).get
      val expressionLms = for (lm <- referenceLandmarks) yield {
        val id = neutralRef.pointSet.findClosestPoint(lm.point).id
        lm.copy(point = expressionRef.pointSet.point(id))
      }


      LOrealDataProvider.incoming.reference.saveLineLandmarks(expression, expressionLms)
    }
  }

}
