package tools

import java.io.File

import breeze.linalg.{DenseMatrix, DenseVector}
import ch.unibas.cs.gravis.facepipeline.LOrealDataProvider
import ch.unibas.cs.gravis.facepipeline.LOrealDataProvider.{Surprise, Neutral, Fear, Anger, Disgust, Joy, Sadness}
import faces.landmarks.{TLMSLandmarksIO, TLMSLandmark3D}
import scalismo.statisticalmodel.MultivariateNormalDistribution
import scalismo.ui.api.ScalismoUI

/**
  * Created by luetma00 on 03.02.17.
  */
object PrepareReferenceLandmarks {

  def main(args : Array[String]) : Unit = {
    scalismo.initialize()


    val rawRefLmsFile = new File(LOrealDataProvider.repositoryRoot.jfile, "data/incoming/reference/landmarks/mean2012_l7_head.tlms")

    // First we load the landmarks of the reference and then set reasonable default uncertainties.
    val referenceLandmarksTLMS = TLMSLandmarksIO.read3D(rawRefLmsFile).get
    val referenceLandmarks = for (lmTlms <- referenceLandmarksTLMS if lmTlms.visible) yield {
      val lm = lmTlms.toLandmark
      val noiseVariance = lm.id.trim match {
        case lmid if lmid.contains("eyebrow") => 3.0
        case lmid if lmid.contains("eye.bottom") => 3.0
        case lmid if lmid.contains("eye.top") => 3.0
        case lmid if lmid.contains("ear") => 5.0
        case _ => 1.0
      }
      lm.copy(uncertainty = Some(MultivariateNormalDistribution(DenseVector.zeros[Double](3), DenseMatrix.eye[Double](3) * noiseVariance)))
    }

    // Transfer the reference landmarks to all the expressions and save them.
    for (expression <- Seq(Neutral)) {

      val neutralRef = LOrealDataProvider.incoming.reference.loadMesh(Neutral).get
      val expressionRef = LOrealDataProvider.incoming.reference.loadMesh(expression).get
      val expressionLms = for (lm <- referenceLandmarks) yield {
        val id = neutralRef.pointSet.findClosestPoint(lm.point).id
        lm.copy(point = expressionRef.pointSet.point(id))
      }


      LOrealDataProvider.incoming.reference.saveLandmarks(expression, expressionLms)
    }
  }

}
