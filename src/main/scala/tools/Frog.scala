/*
 * Copyright University of Basel, Graphics and Vision Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tools

import java.io.FileWriter

import faces.parameters._
import faces.render.WindowTransform
import scalismo.faces.color.RGB
import scalismo.geometry.{Point, Vector}
import tools.ini.{Ini, Mapper, Printer}

import scala.util.{Failure, Success, Try}

class Frog(val ini: Ini) {

  require(ini.getBlock("default").isDefined)

  val default: Map[String, String] = ini.getBlock("default").get

  def value(key: String): Option[Double] = {
    default.get(key).map(_.toDouble)
  }

  def prettyPrint(): String = {
    Printer.printPairs(Mapper.pairListFromMap(default))
  }

  def directionalLight(): DirectionalLight = DirectionalLight(
    ambient = RGB(value("ambientR").get, value("ambientG").get, value("ambientB").get),
    diffuse = RGB(value("directionalR").get, value("directionalG").get, value("directionalB").get),
    specular = RGB(value("directionalR").get, value("directionalG").get, value("directionalB").get),
    direction = Vector(value("directionX").get, value("directionY").get, value("directionZ").get),
    shininess = value("s").get
  )

  def camera(): Camera = {
    Camera(
      focalLength = value("f").get,
      principalPoint = Point(value("oX").get*2.0/imageSize().width, value("oY").get*2.0/imageSize().height),
      sensorSize = Vector(value("right").get*2*100, value("top").get*2*100),
      near = value("near").get,
      far = value("far").getOrElse(1.0E5),
      orthographic = false
    )
  }

  def pose(): Pose = {
    Pose(
      scaling = 1.0,
      translation = Vector(value("transX").get, value("transY").get, value("transZ").get),
      roll = value("rotZ").get,
      yaw = -value("rotY").get, // frog files have inverted y-axis
      pitch = value("rotX").get
    )
  }

  def view(): ViewParameter = {
    ViewParameter(
      translation = Vector(0,0,0),
      roll = 0,
      yaw = 0, // frog files have inverted y-axis
      pitch = 0
    )
  }

  def windowTransform(): WindowTransform = {
    WindowTransform(
      width = value("width").get.toInt,
      height = value("height").get.toInt,
      near = value("near").get,
      far = value("far").getOrElse(1.0E5)
    )
  }

  def imageSize(): ImageSize = {
    ImageSize(
      width = value("width").get.toInt,
      height = value("height").get.toInt
    )
  }

  def rp(): RenderParameter = {
    RenderParameter.default.copy(pose = pose(), camera = camera())
  }

}

object Frog {

  def readFromFile(file: String): Try[Frog] = {

    val ini = Ini.readFromFile(file)

    ini match {
      case scalaz.Success(content) => Success(new Frog(content))
      case scalaz.Failure(msgs) => Failure(new Exception(msgs.list.toList mkString "\n"))
    }
  }

  def writeToFile(ini: Ini, file: String): Try[Unit] = {
    val writer = new FileWriter(file)
    writer.write(ini.prettyPrint)
    writer.write('\n')
    writer.close()
    Success(Unit)
  }
}

