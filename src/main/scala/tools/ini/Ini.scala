/*
 * Copyright University of Basel, Graphics and Vision Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tools.ini

import scala.io.Source
import scalaz.{Generator, _}

class Ini(val blocks: Map[String, Map[String, String]]) {

  def prettyPrint = Printer.print(this)

  def getBlock(name: String): Option[Map[String, String]] = {
    blocks.get(name)
  }

  def getValue(name: String): Option[String] = {
    blocks.get("default") match {
      case Some(block) => block.get(name)
      case _ => None
    }
  }
}

object Ini{

  def readFromString(string: String): ValidationNel[String, Ini] = {
    for {
      content <- IniParser.parse(string)
    } yield Mapper.iniFromContent(content)
  }

  def readFromSource(source: Source) = {
    readFromString(source.mkString)
  }

  def readFromFile(name: String) = {
    readFromSource(Source.fromFile(name))
  }

}