/*
 * Copyright University of Basel, Graphics and Vision Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tools.ini

import IniParser.{Content, Block, Pair}

private[tools] object Mapper {

  def iniFromContent(ini: Content): Ini = {
    new Ini(blockMapFromList(ini.blocks))
  }

  def contentFromIni(ini: Ini): Content = {
    Content(blockListFromMap(ini.blocks))
  }

  def blockMapFromList(ss: List[Block]): Map[String, Map[String,String]] = {
    ss.map { case Block(sn, kvs) => (sn -> pairMapFromList(kvs)) } toMap
  }

  def blockListFromMap(ss: Map[String, Map[String,String]]): List[Block] = {
    ss.map { case (sn, s) => Block(sn, pairListFromMap(s)) } toList
  }

  def pairMapFromList(kvs: List[Pair]): Map[String, String] = {
    kvs.map { case Pair(k, v) => k -> v } toMap
  }

  def pairListFromMap(kvs: Map[String, String]): List[Pair] = {
    kvs.map { case (k, v) => Pair(k, v) } toList
  }
}
