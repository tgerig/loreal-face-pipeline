/*
 * Copyright University of Basel, Graphics and Vision Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tools.ini

import scala.util.parsing.combinator.JavaTokenParsers
import scalaz._
import Scalaz._

private[ini] object IniParser extends JavaTokenParsers {

  def parse(in: String): ValidationNel[String, Content] = {
    parseAll(iniFile, in) match {
      case Success(ini, _) => ini.successNel
      case NoSuccess(msg, _) => msg.failureNel
    }
  }

  type BlockName = String
  type Key = String
  type Value = String

  case class Pair(key: Key, value: Value)
  case class Block(name: BlockName, keyValues: List[Pair])
  case class Content(blocks: List[Block])

  override val whiteSpace = """[ \t]+""".r
  lazy val eol = """(\r?\n)|(\z\n)""".r
  lazy val comment = """;.*""".r
  lazy val value = ( ident | floatingPointNumber )

  lazy val keyValue = ident ~ ("=" ~> value <~ eol.*) ^^ {
    case (k ~ v) => Pair(k, v)
  }

  lazy val keyValues = (eol *) ~> (keyValue +) <~ (eol *) ^^ {
    case kvs => kvs
  }

  lazy val default = keyValues ^^ {
    case kvs => Block("default",kvs)
  }

  lazy val block = ("[" ~> ident <~ "]") ~ keyValues ^^ {
    case (n ~ kvs) => Block(n, kvs)
  }

  lazy val blocks = (eol *) ~> (block +) <~ (eol *) ^^ {
    case ss => ss
  }

  lazy val iniFile = (eol *) ~> (default ?) ~ (block *) <~ (eol *) ^^ {
    case (start ~ rest) => {
      Content(
        start match {
          case Some(s) => s +: rest
          case None => rest
        }
      )
    }
  }
}
