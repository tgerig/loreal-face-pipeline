/*
 * Copyright University of Basel, Graphics and Vision Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tools

import java.io.{File, PrintWriter}

import ch.unibas.cs.gravis.facepipeline.{ExpressionType, LOrealDataProvider}
import ch.unibas.cs.gravis.facepipeline.LOrealDataProvider.Person
import faces.mesh.{ColorNormalMesh3D, GravisMeshIO, VertexColorMesh3D}
import faces.mesh.io.PLYMesh
import faces.momo.ModelIO
import faces.parameters.{ImageSize, ParametricRenderer, RenderParameter}
import faces.render.{Transform3D, TriangleFilters, TriangleRenderer, ZBuffer}
import scalismo.common.UnstructuredPointsDomain
import scalismo.faces.color.RGBA
import scalismo.faces.image.PixelImageIO
import scalismo.geometry.{Point, Vector, _3D}
import scalismo.mesh.{SurfacePointProperty, _}
import scalismo.registration.LandmarkRegistration

import scala.reflect.io.Path
import scala.util.Success

object LOrealVisualizer extends App {

  scalismo.initialize()

  val clownColor = PLYMesh.readVertexColorMesh3D("/export/faces/projects/pami-ppm2017/data/models/expression-means/mean2012_l7_bfm_nomouth_largerLips.ply").get.color
  val model = ModelIO.loadMoMo(new File("/export/faces/model/model2015.1/model/model2015-1_l7_bfm_nomouth_rms.h5"),"/").get
  val reference = GravisMeshIO.readMSHMesh(new File("/export/faces/model/model2015.1/reference/mean2012_l7_bfm_nomouth.msh.gz")).get.triangleMesh
  val refPoints = reference.pointSet.points.toIndexedSeq
  val parameter = RenderParameter.defaultSquare.copy(
    imageSize = ImageSize(1024,1024)
  )

  val outDir = Path("/export/faces/projects/pami-ppm2017/loreal-face-pipeline/data/modelbuilding/visualization")


//  www()
  run(visualization)

  def run[T](function: (Person, ExpressionType) => T) = {
    LOrealDataProvider.Expressions.expressionList().map { exp =>
      LOrealDataProvider.registration.ids(exp).map { id =>
        function(id, exp)
      }
    }
  }

  def visualization(id: Person, exp: ExpressionType) = {
    try {
    val p = Path("/export/faces/projects/pami-ppm2017/loreal-face-pipeline/data")
    val unscaledTargets = Seq(GravisMeshIO.readMSHMesh((p/s"incoming/mesh/${id.id}_1.msh.gz").jfile).get.colorNormalMesh.get,
      GravisMeshIO.readMSHMesh((p/s"incoming/mesh/${id.id}_2.msh.gz").jfile).get.colorNormalMesh.get)
    val targets = unscaledTargets.map( t => t.copy(shape = t.shape.copy( pointSet = UnstructuredPointsDomain(t.shape.pointSet.points.map(pt => (pt.toVector*1000.0).toPoint).toIndexedSeq))))


    LOrealDataProvider.LOrealModelBuilding.loadColoredMesh(id, exp) match {

      case Success(meterMesh) =>

        val mesh = meterMesh.copy(shape = meterMesh.shape.copy(pointSet = UnstructuredPointsDomain(meterMesh.shape.pointSet.points.map(pt => (pt.toVector*1000.0).toPoint).toIndexedSeq)))

        val t = LandmarkRegistration.rigid3DLandmarkRegistration(mesh.shape.pointSet.points.toIndexedSeq.zip(refPoints), Point(0, 0, 0))

        val trans = new Transform3D {
          override def apply(x: Point[_3D]): Point[_3D] = t(x)

          override def apply(v: Vector[_3D]): Vector[_3D] = t.rotation(v.toPoint).toVector
        }

        val aligned = mesh.transform(trans)
        val alignedTargets = targets.map( tar => tar.transform(trans))

        renderTextureExtracted(aligned,id,exp)
        renderClownColor(aligned,id,exp)
        renderShape(aligned,id,exp)
        renderTargets(alignedTargets,id,exp)
        renderCaricatur(model.mean,aligned,id,exp,1.5)
        renderCaricatur(model.mean,aligned,id,exp,2.0)
        renderCaricatur(model.mean,aligned,id,exp,3.0)

      case _ =>
    }

    } catch {
      case e: Throwable => println(s"\tError with $id - $exp"); println("\t%s".format(e.toString))
    }

  }


  def buildName(id: Person, exp: ExpressionType) = s"${id.id}${exp.toString}"
  def titleCell(str: String) = "<th align=\"center\"><font size=\"9\">%s</font></th>".format(str)
  def tableCell(str: String) = "<td align=\"left\" style=\"background-color:darkgray;\">%s</td>".format(str)
  def image(src: String) = "<img src=\"%s\" height=512px>".format(src)
  def link(target: String, content: String) = "<a href=\"%s\">%s</a>".format(target, content)
  def back(target: String) = "<font size=\"14\"><a href=\"%s\">back</a></font>".format(target)

  def startHTML() = "<html><body>\n"
  def startTable() = "<table>\n"
  def startRow() = "<tr>\n"
  def endRow() = "</tr>\n"
  def endTable() = "</table>\n"
  def endHTML() = "</body></html>\n"

  def header() = {
    startRow() +
      titleCell("target") +
      titleCell("VC") +
      titleCell("shape") +
      titleCell("clown") +
      titleCell("image") +
      endRow()
  }

  def www() = {

    val index = outDir / s"index.html"
    val writer = new PrintWriter(index.jfile)

    val listing: Seq[(Person, ExpressionType)] = run((a, b) => (a, b)).flatten

    writer.write(
      startHTML() +
        startTable() +
        listing.grouped(100).zipWithIndex.map {
          case (list, nbr) =>
            writeOverviewPart(list, nbr)
            startRow() +
              tableCell(link(s"index_${nbr}.html", s"part ${nbr}")) +
              endRow()

        }.mkString("\n") +
        endTable() +
        endHTML()
    )

    writer.flush()
    writer.close()

  }

  def writeOverviewPart(listing: Seq[(Person, ExpressionType)], nbr: Int): Unit = {
    val index = outDir / s"index_${nbr}.html"
    val writer = new PrintWriter(index.jfile)
    writer.write(
      startHTML() +
        back(s"index.html") +
        startTable() +
        listing.grouped(4).map { list =>
          startRow() +
            list.map {
              case (id, exp) =>
                val name = buildName(id, exp)

                writeIndexForScan(id, exp, nbr)

                tableCell(link(s"index_$name.html", image(s"$name.png")))
            }.mkString("\n") +
            endRow()
        }.mkString("\n") +
        endTable() +
        back(s"index.html") +
        endHTML()
    )
    writer.flush()
    writer.close()
  }

  def writeIndexForScan(id: Person, exp: ExpressionType, nbr: Int): Unit = {
    val name = buildName(id, exp)
    val pindex = outDir / s"index_${name}.html"
    val pwriter = new PrintWriter(pindex.jfile)
    pwriter.write(
      startHTML() +
        back(s"index_${nbr}.html") +
        startTable() +
        header() +

        startRow() +
        tableCell(image(s"${name}_target.png")) +
        tableCell(image(s"${name}.png")) +
        tableCell(image(s"${name}_shape.png")) +
        tableCell(image(s"${name}_clown.png")) +
        tableCell(image(s"/export/faces/data_loreal/registration/dataset_001/marked/${id.id}/cleaned/pod_1_tex.png")) +
        tableCell(image(s"/export/faces/data_loreal/registration/dataset_001/marked/${id.id}/cleaned/pod_2_tex.png")) +
        endRow() +

        Seq(1.5, 2.0, 3.0).map { scale: Double =>
          startRow() +
            tableCell("<font size=\"18\">Caricatur %s</font>".format("%01.1f".format(scale))) +
            tableCell(image(s"${name}_colorCar${"%01.1f".format(scale)}.png")) +
            tableCell(image(s"${name}_shapeCar${"%01.1f".format(scale)}.png")) +
            tableCell(image(s"${name}_shapeCarClown${"%01.1f".format(scale)}.png")) +
            tableCell("") +
            tableCell("") +
            endRow()
        }.mkString("\n") +

        endTable() +
        back(s"index_${nbr}.html") +
        endHTML()
    )
    pwriter.flush()
    pwriter.close()
  }



  def renderTextureExtracted(mesh: ColorNormalMesh3D, id: Person, exp: ExpressionType) = {
    val image = ParametricRenderer.renderParameterMesh(
      parameter,
      mesh,
      RGBA.BlackTransparent
    )

    val imgPath = outDir / s"${id.id}${exp.toString}.png"
    PixelImageIO.write(image, imgPath.jfile)
  }

  def renderTargets(meshs: Seq[ColorNormalMesh3D], id: Person, exp: ExpressionType) = {

    val buffer = ZBuffer(parameter.imageSize.width, parameter.imageSize.height, RGBA.BlackTransparent)

    val worldMesh = meshs.map(_.transform(parameter.modelViewTransform))
    val bfcf = worldMesh.map(wm => TriangleFilters.backfaceCullingFilter(wm.shape, parameter.view.eyePosition))

    meshs.zip(bfcf).foreach{ case (mesh, backfaceCullingFilter) =>
      TriangleRenderer.renderMesh(
        mesh.shape,
        backfaceCullingFilter,
        parameter.pointShader,
        parameter.imageSize.screenTransform,
        parameter.pixelShader(mesh),
        buffer)
    }

    val image = buffer.toImage

    val imgPath = outDir / s"${id.id}${exp.toString}_target.png"

    PixelImageIO.write(image, imgPath.jfile)
  }

  def renderShape(mesh: ColorNormalMesh3D, id: Person, exp: ExpressionType) = {
    val image = ParametricRenderer.renderParameterMesh(
      parameter,
      mesh.copy(color = new MeshSurfaceProperty[RGBA] {
        override def onSurface(triangleId: TriangleId, bcc: BarycentricCoordinates): RGBA = RGBA.White
        override def triangulation: TriangleList = mesh.shape.triangulation
      }),
      RGBA.BlackTransparent
    )

    val imgPath = outDir / s"${id.id}${exp.toString}_shape.png"
    PixelImageIO.write(image, imgPath.jfile)
  }

  def renderClownColor(mesh: ColorNormalMesh3D, id: Person, exp: ExpressionType) = {
    val image = ParametricRenderer.renderParameterMesh(
      parameter,
      mesh.copy(color = clownColor),
      RGBA.BlackTransparent
    )

    val imgPath = outDir / s"${id.id}${exp.toString}_clown.png"
    PixelImageIO.write(image, imgPath.jfile)
  }




  def renderCaricatur(mean: VertexColorMesh3D, mesh: ColorNormalMesh3D, id: Person, exp: ExpressionType, scale: Double = 1.5) = {

    val shape: TriangleMesh3D = mesh.shape
    val triangulation = shape.triangulation

    val pointCaricatur = {
      mesh.shape.pointSet.points.zip(mean.shape.pointSet.points).map { case (p, m) =>
        val v = p - m
        m + (v * scale)
      }.toIndexedSeq
    }

    val shapeCaricatur = TriangleMesh3D( pointCaricatur, triangulation)

    val color = SurfacePointProperty.sampleSurfaceProperty[RGBA](mesh.color, _.head )
    val colCar = color.pointData.zip(mean.color.pointData).map{ case(c,m) =>
      val d = c - m
      (m + (d*scale)).clamped
    }
    val colorCaricatur = SurfacePointProperty[RGBA](triangulation,colCar)
    val whiteColor = SurfacePointProperty[RGBA](triangulation,mean.color.pointData.map(_ => RGBA.White))

    def makeImage(mesh: VertexColorMesh3D, name: String) = {
      val image = ParametricRenderer.renderParameterMesh(
        parameter,
        ColorNormalMesh3D(mesh),
        RGBA.BlackTransparent
      )

      val imgPath = outDir / name
      PixelImageIO.write(image, imgPath.jfile)
    }


    makeImage(
      VertexColorMesh3D(shapeCaricatur,whiteColor)
      ,s"${buildName(id,exp)}_shapeCar${"%01.1f".format(scale)}.png"
    )

    makeImage(
      VertexColorMesh3D(shape,colorCaricatur)
      ,s"${buildName(id,exp)}_colorCar${"%01.1f".format(scale)}.png"
    )

    makeImage(
      VertexColorMesh3D(shapeCaricatur, clownColor),
      s"${buildName(id, exp)}_shapeCarClown${"%01.1f".format(scale)}.png"
    )
  }



}
