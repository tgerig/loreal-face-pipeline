package local

import java.io.File

import ch.unibas.cs.gravis.facepipeline.LOrealDataProvider
import ch.unibas.cs.gravis.facepipeline.LOrealDataProvider.Neutral
import scalismo.io.MeshIO

/**
  * Created by luetma00 on 03.02.17.
  */
object ConvertRef extends App {
  scalismo.initialize()
  MeshIO.writeMesh(LOrealDataProvider.incoming.reference.loadMesh(Neutral).get, new File("/home/luetma00/tmp/reference-l7.vtk")).get

}
