package modelbuilding

import java.io.{File, FilenameFilter}

import breeze.linalg.DenseMatrix
import ch.unibas.cs.gravis.facepipeline.{ExpressionType, LOrealDataProvider}
import ch.unibas.cs.gravis.facepipeline.LOrealDataProvider.{LOrealModelBuilding, Neutral, Person}
import faces.mesh.{VertexColorMesh3D, _}
import faces.momo.MoMo
import faces.momo.MoMo.NeutralWithExpression
import faces.parameters.io.RenderParametersIO
import faces.render.{PointShader, Projection, RenderTransforms, Transform3D}
import modelbuilding.textureExtraction.{CCamera, MultiTextureExtraction, ScannerTsaiParameters, TextureBlending}
import scalismo.common._
import scalismo.faces.color.{RGB, RGBA}
import scalismo.faces.image.AccessMode.Repeat
import scalismo.faces.image.{PixelImageDomain, PixelImageIO}
import scalismo.geometry._
import scalismo.kernels.{DiagonalKernel, GaussianKernel, MatrixValuedPDKernel}
import scalismo.mesh.{SurfacePointProperty, TriangleMesh, TriangleMesh3D}
import scalismo.numerics.PivotedCholesky
import scalismo.numerics.PivotedCholesky.NumberOfEigenfunctions
import scalismo.registration.{LandmarkRegistration, RigidTransformation, Transformation}
import scalismo.statisticalmodel.dataset.{DataCollection, DataItem}
import scalismo.statisticalmodel.{DiscreteLowRankGaussianProcess, GaussianProcess, PancakeDLRGP}
import tools.Frog

import scala.reflect.io.Path
import scala.util.{Failure, Success, Try}

/**
 * Created by blumcl02 on 30.01.17.
 */

object ModelBuilding {

  def main(args: Array[String]) {
    scalismo.initialize()

    run()
  }

  def run() {
    createMeshesWithVertexColor()
    //    buildMoMoExpress()
  }

  def createMeshesWithVertexColor(): Unit = {

    val w = 2048
    val h = 1024

    val mshTextureEmbeddingMesh = GravisMSH.Reader.readMSHMesh(new File("/export/faces/model/model2015.1/reference/mean2012_l7_bfm_pascaltex.msh.gz"))
    val woMouth = GravisMSH.Reader.readMSHMesh(new File("/export/faces/model/model2015.1/reference/mean2012_l7_bfm_nomouth.msh.gz"))
    val mask: BinaryMask = BinaryMask.createFromMeshes(mshTextureEmbeddingMesh.triangleMesh, woMouth.triangleMesh)
    val reducer = mshTextureEmbeddingMesh.triangleMesh.operations.mask(mask, _ => true)

    val textureMapping = mshTextureEmbeddingMesh.getSingleTextureColor.get.textureMapping
    val mapping = reducer.applyToSurfaceProperty(textureMapping)

    val textureDomain = PixelImageDomain(w, h)

    println("Extracting vertex color for meshes ...")

    LOrealDataProvider.Expressions.expressionList().foreach { exp =>

      LOrealDataProvider.registration.ids(exp).foreach { id =>

        if ( ! LOrealModelBuilding.meshPath(id,exp).jfile.exists() ) {
          println(s"\textracting VC for ${id.id}$exp")
          try {
            val registeredShape = LOrealDataProvider.registration.loadMesh(id, exp).get

            require(
              reducer.transformedMesh.triangulation.triangles.size == registeredShape.triangulation.triangles.size,
              s"Triangulation of reduced mapping and the registered mesh are not of the same size. mapping(${reducer.transformedMesh.triangulation.triangles.size}) / registerd(${registeredShape.triangulation.triangles.size})"
            )

            //        val targetMeshes = loadMeshes(id,exp)

            val frogs = loadFrogs(id, exp)
            //        val jsons = loadJsons(id, exp)
            val cams = for (frogTry <- frogs) yield {
              val frog = frogTry.get
              new CCamera {

                def objectTransform = frog.pose.transform

                def objectTransformInv = frog.pose.transform.inverted

                def modelView = frog.view.viewTransform

                def modelViewInv = frog.view.viewTransform.inverted

                def projection = frog.camera.projection

                def windowTransform = frog.imageSize.screenTransform

                def windowTransformInv = frog.imageSize.screenTransform.inverted

                val focalLength = frog.value("f").get

                override def worldToImage(point: Point[_3D]): Point[_2D] = {
                  val pt = windowTransform(projection(modelView(objectTransform(point))))
                  Point(pt.x, pt.y)
                }

                override def cameraTransform(point: Point[_3D]): Point[_3D] = objectTransformInv(modelView(point))

                override def imageToWorld(point: Point[_2D]): Point[_3D] = objectTransformInv(modelViewInv(projection.inverse(windowTransformInv(Point(point.x, point.y, focalLength)))))
              }
            }

            val imgs = loadImages(id, exp)

            val (fusedTexture, _) = MultiTextureExtraction.extractTextureFromMultipleSources(textureDomain, cams, imgs, mapping, registeredShape, TextureBlending.blendTextures)

            val texturedMesh: ColorNormalMesh3D = ColorNormalMesh3D(registeredShape, TextureMappedProperty(registeredShape.triangulation, mapping, fusedTexture.withAccessMode(Repeat())), registeredShape.vertexNormals)
            val vertexColor = SurfacePointProperty.sampleSurfaceProperty(texturedMesh.color, (a: IndexedSeq[RGBA]) => a.head)

            val registeredShapeWithVertexColor = ColorNormalMesh3D(registeredShape, vertexColor, registeredShape.vertexNormals)
            LOrealModelBuilding.saveColoredMesh(id, exp, mesh = registeredShapeWithVertexColor)
          } catch {
            case e: Throwable =>
              println("=== ERROR ===")
              println(e)
          }
        } else {
          println(s"\talready extracted VC for ${id.id}$exp")
        }
      }

    }

  }

  val incomingLocation = Path("/export/faces/projects/pami-ppm2017/loreal-face-pipeline/data/incoming/mesh/")

  def loadMeshes(id: Person, exp: ExpressionType) = {
    incomingLocation.jfile.list(new FilenameFilter {
      override def accept(dir: File, name: String): Boolean = {
        name.contains(id.id) && name.contains(exp.toString) && name.endsWith(".msh.gz")
      }
    }).map { filename =>
      GravisMeshIO.readMSHMesh(new File((incomingLocation / filename).toString())).get
    }
  }

  def loadFrogs(id: Person, exp: ExpressionType) = {
    (1 to 2).map { nbr =>
            Frog.readFromFile((incomingLocation / s"${id.id}${exp.toString}_$nbr.frog").toString())
//      RenderParametersIO.fromFile(new File((Path("/tmp") / s"${id.id}${exp.toString}_$nbr.json").toString()))
    }
  }

  def loadJsons(id: Person, exp: ExpressionType) = {
    (1 to 2).map { nbr =>
      //      Frog.readFromFile((originalBu3dDataLocation / s"${id.id}${exp.toString}_$nbr.frog").toString())
      RenderParametersIO.fromFile(new File((Path("/tmp") / s"${id.id}${exp.toString}_$nbr.json").toString()))
    }
  }

  def loadImages(id: Person, exp: ExpressionType) = {
    (1 to 2).map { nbr =>
      PixelImageIO.read[RGBA](new File((Path("/export/faces/data_loreal/registration/dataset_001/marked") / s"${id.id}/cleaned/${id.id}${exp.toString}_$nbr.png").toString())).get
    }
  }

  val COLOR_ALPHA_THRESHOLD = 1.0

  /**
   * Build a model from meshes with vertex color.
   */
  def buildMoMoExpress(): Unit = {
    println("Building model from meshes with vertex color...")

    val referenceModel: MoMo = MoMo.load(new File("/export/faces/model/model2015.1/model/model2015-1_l7_bfm_nomouth_pca.h5")).get // todo: we need to think about the data we provide here
    val reference = referenceModel.referenceMesh

    val expressionList = LOrealDataProvider.Expressions.expressionList().filter(exp => exp != Neutral)
    val otherExpressions = expressionList.map(exp => (exp, LOrealDataProvider.registration.ids(exp)))
    val IDs = LOrealDataProvider.registration.ids(LOrealDataProvider.Neutral)

    val data: Seq[Try[(DiscreteField[_3D, RGBA], VertexColorMesh3D, Seq[Try[NeutralWithExpression]])]] = IDs.zipWithIndex.map {
      case (id, idx) =>
        println(s"... loading data for ${id.id} (${idx + 1}/${IDs.size})")

        for {
          unalignedMesh <- LOrealModelBuilding.loadColoredMesh(id, LOrealDataProvider.Neutral)
        } yield {

          val transform = calculateShapeAligningTransform(unalignedMesh.shape, reference)
          val mesh = unalignedMesh.transform(transform)

          println(s"... ... ${id.id} $Neutral")
          val pointSet = mesh.shape.pointSet
          val vertexColor = mesh.color match {
            case spp: SurfacePointProperty[RGBA] => spp
            case _ => throw new Exception("Unexpected color property of mesh!!!")
          }

          val vcm = VertexColorMesh3D(mesh.shape, SurfacePointProperty.sampleSurfaceProperty(mesh.color, _.head))
          val availableExpressions = otherExpressions.flatMap {
            case (exp, seq) =>
              val shortID = id.id.substring(0, 6)
              seq.filter(p => p.id.contains(shortID)).map(p => (exp, p))
          }
          val neutralWithExpression = availableExpressions.par.map {
            case (exp, scan) =>
              println(s"... ... ${scan.id} ${exp}")
              for (unalignedExpression <- LOrealModelBuilding.loadColoredMesh(scan, exp)) yield {

                val transform = calculateShapeAligningTransform(unalignedExpression.shape, reference)
                val alignedExpression = unalignedExpression.transform(transform)

                val vcme = VertexColorMesh3D(alignedExpression.shape, SurfacePointProperty.sampleSurfaceProperty(alignedExpression.color, _.head))
                NeutralWithExpression(vcm, vcme)
              }
          }.toIndexedSeq

          (
            DiscreteField[_3D, RGBA](pointSet, vertexColor.pointData),
            vcm,
            neutralWithExpression
          )
        }
    }

    val colors: Seq[DiscreteField[_3D, RGBA]] = data.collect({ case Success(e) => e._1 })
    val vertexColorMeshs: Seq[VertexColorMesh3D] = data.collect({ case Success(e) => e._2 })
    val neutralsWithExpression: Seq[NeutralWithExpression] = data.collect({ case Success(e) => e._3.collect { case Success(exp) => exp } }).flatten

    println(".. data loaded ...")

    val momo = MoMo.buildFromRegisteredSamples(reference, vertexColorMeshs.toIndexedSeq, vertexColorMeshs.toIndexedSeq, neutralsWithExpression.toIndexedSeq, 0, 0, 0)
    LOrealModelBuilding.saveModel(LOrealDataProvider.BFM, momo) match {
      case Success(_) =>
      case Failure(e) => println(e)
    }

    println("... initial model is built - (not handling missing color) ...")
    println(s"... ... shape rank: ${momo.shape.rank}")
    println(s"... ... color rank: ${momo.color.rank}")
    println(s"... ... exp rank: ${momo.expression.rank}")

    val colorModel = buildColorModel(reference, colors.toIndexedSeq, colors.size - 1)
    println("... color model is built ...")

    val bu3dModel = MoMo.apply(momo.referenceMesh, momo.shape, colorModel, momo.expression, momo.landmarks)
    LOrealModelBuilding.saveModel(LOrealDataProvider.BFM, bu3dModel) match {
      case Success(_) =>
      case Failure(e) => println(e)
    }
    println("... model building finished!")
    try {
      Thread.sleep(6000)
    } catch {
      case e: Throwable =>
    }

  }

  def calculateShapeAligningTransform(mesh: TriangleMesh3D, target: TriangleMesh3D): Transform3D = {
    val t: RigidTransformation[_3D] = LandmarkRegistration.rigid3DLandmarkRegistration(mesh.pointSet.points.zip(target.pointSet.points).toSeq, Point(0, 0, 0))

    val transform = new Transform3D {
      override def apply(x: Point[_3D]): Point[_3D] = t(x)

      override def apply(v: Vector[_3D]): Vector[_3D] = t.rotation(v.toPoint).toVector
    }

    transform
  }

  def transferLandmarksAssumingCorrespondence(model: MoMo, landmarks: Map[String, Landmark[_3D]]): Map[String, Landmark[_3D]] = {
    val refPoints = model.referenceMesh.pointSet
    landmarks.map {
      case (s, lm) =>
        val oldPosition = lm.point
        val pid = refPoints.findClosestPoint(oldPosition).id
        val newPosition = model.referenceMesh.pointSet.point(pid)
        (s, Landmark(lm.id, newPosition, lm.description, lm.uncertainty))
    }
  }

  /**
   * Builds a color model. This model building accounts for missing color values.
   *
   * @param referenceMesh Reference mesh.
   * @param colorFields Colorfields to build the model from.
   * @param numberOfComponents Number of desired components.
   * @return Color model.
   */
  def buildColorModel(
    referenceMesh: TriangleMesh3D,
    colorFields: IndexedSeq[DiscreteField[_3D, RGBA]],
    numberOfComponents: Int
  ): PancakeDLRGP[_3D, RGB] = {

    val domain = referenceMesh.pointSet

    val meanRGBA: DiscreteField[_3D, RGBA] = DiscreteField(domain, saveMean(colorFields.map(_.data)))
    val meanRGB = DiscreteField[_3D, RGB](domain, meanRGBA.data.map(_.toRGB))
    val meanFreeColors: Seq[DiscreteField[_3D, RGBA]] = saveMeanFreeColors(colorFields.map(_.data), meanRGBA.data).map { a => DiscreteField(domain, a) }

    val kernel: MatrixValuedPDKernel[_3D] = MissingEntryKernel(meanFreeColors)
    val gp: GaussianProcess[_3D, RGB] = GaussianProcess(meanRGB.interpolateNearestNeighbor(), kernel)
    val grf: (DiscreteLowRankGaussianProcess[_3D, RGB], UnstructuredPointsDomain[_3D]) = approximatePointSet(domain, gp, NumberOfEigenfunctions(numberOfComponents))

    val colorPC = PancakeDLRGP(grf._1)
    colorPC
  }

  /**
   * Calculates the mean based on available samples only.
   * If no value is available, i.e. the alpha channel is zero for all samples at a given vertex, BlackTransparent is set as mean color.
   */
  def saveMean(colorVectors: Seq[IndexedSeq[RGBA]]): IndexedSeq[RGBA] = {
    //    assert(fields.forall(f => f.domain.points.toArray.deep == fields.head.domain.points.toArray.deep))
    val numberOfColorValues = colorVectors.size
    val numberOfSamples = colorVectors.head.size

    val accumulatedColor = Array.fill(numberOfSamples)(RGBA.BlackTransparent)
    val numberOfusedColors = Array.fill(numberOfSamples)(0)
    for (
      i <- 0 until numberOfColorValues;
      j <- 0 until numberOfSamples
    ) {
      val color = colorVectors(i)(j)
      if (color.a == 1.0) {
        accumulatedColor(j) = accumulatedColor(j) + color
        numberOfusedColors(j) += 1
      }
    }
    val mean = accumulatedColor.zip(numberOfusedColors).map {
      case (sumOfColors, counter) =>
        if (counter == 0) RGBA.BlackTransparent
        else sumOfColors / counter
    }
    mean
  }

  /**
   * Substracts the mean color vector from all color samples. Else BlackTransparent.
   */
  def saveMeanFreeColors(colorVectors: Seq[IndexedSeq[RGBA]], meanColorVector: IndexedSeq[RGBA]): Seq[IndexedSeq[RGBA]] = {
    colorVectors.map { colorVector =>
      colorVector.zip(meanColorVector).map {
        case (color, meanColor) =>
          if (color.a < COLOR_ALPHA_THRESHOLD) {
            RGBA.BlackTransparent
          } else {
            val d = RGBA(color.r - meanColor.r, color.g - meanColor.g, color.b - meanColor.b, color.a)
            d
          }
      }
    }
  }

  /**
   * Covariance kernel that has a backupkernel used when some of the data is missing.
   *
   * @param colorFields Input data with possibly some data missing.
   */
  case class MissingEntryKernel(colorFields: Seq[DiscreteField[_3D, RGBA]]) extends MatrixValuedPDKernel[_3D] {

    val backupKernel = DiagonalKernel(GaussianKernel[_3D](30), 3) * 0.0001

    val fs = colorFields.map(f => f.interpolateNearestNeighbor())

    override protected def k(x: Point[_3D], y: Point[_3D]): DenseMatrix[Double] = {

      val good = fs.foldLeft(DenseMatrix.zeros[Double](outputDim, outputDim)) { (sum, field) =>
        val xc = field(x)
        val yc = field(y)
        val addend =
          if (xc.a < COLOR_ALPHA_THRESHOLD && yc.a < COLOR_ALPHA_THRESHOLD) {
            backupKernel(x, y)
          } else {
            xc.toRGB.toVector.outer(yc.toRGB.toVector).toBreezeMatrix
          }
        sum + addend * (1.0 / fs.size)
      }

      good
    }

    override def domain: Domain[_3D] = RealSpace[_3D]

    override def outputDim: Int = 3
  }

  /**
   * Funny function addapted from the cholesky approximation
   */
  def approximatePointSet(
    points: UnstructuredPointsDomain[_3D],
    gp: GaussianProcess[_3D, RGB],
    sc: PivotedCholesky.StoppingCriterion
  ): (DiscreteLowRankGaussianProcess[_3D, RGB], UnstructuredPointsDomain[_3D]) = {

    val D = 1.0
    val (phi, lambda) = PivotedCholesky.computeApproximateEig[_3D](gp.cov, points.points.toIndexedSeq, D, sc)

    val nPhi = phi.cols

    val klBasis: DiscreteLowRankGaussianProcess.KLBasis[_3D, RGB] = for (i <- 0 until nPhi) yield {
      val v = DiscreteField[_3D, RGB](points, points.pointsWithId.toIndexedSeq.map(f => phiVec(i, f._2, phi)))
      DiscreteLowRankGaussianProcess.Eigenpair(lambda(i), v)
    }
    val mean = DiscreteField[_3D, RGB](points, points.points.toIndexedSeq.map(p => gp.mean(p)))

    val r = DiscreteLowRankGaussianProcess[_3D, RGB](mean, klBasis)
    (r, points)

  }

  /**
   * Funny helper function 1
   */
  private def phiVec(i: Int, ptID: PointId, phi: DenseMatrix[Double]): RGB = {
    RGB(phiWithDim(i, 0, ptID.id, phi), phiWithDim(i, 1, ptID.id, phi), phiWithDim(i, 2, ptID.id, phi))
  }

  /**
   * Funny helper function 2
   */
  private def phiWithDim(i: Int, dim: Int, ptId: Int, phi: DenseMatrix[Double]) = {
    phi(ptId * 3 + dim, i)
  }

}
