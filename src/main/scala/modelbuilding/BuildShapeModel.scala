package modelbuilding

import java.io.File

import ch.unibas.cs.gravis.facepipeline.LOrealDataProvider
import scalismo.geometry.{Landmark, Point, _3D}
import scalismo.io.StatismoIO
import scalismo.registration.LandmarkRegistration
import scalismo.statisticalmodel.StatisticalMeshModel
import scalismo.statisticalmodel.dataset.DataCollection

/**
  * Created by gerith00 on 03.03.17.
  */

case class LandmarkPair(referenceLandmark: Landmark[_3D], targetLandmark: Landmark[_3D])

object BuildShapeModel {

  private def correspondingLandmarkPairs(referenceLandmarks: Seq[Landmark[_3D]], targetLandmarks: Seq[Landmark[_3D]]): Seq[LandmarkPair] = {

    referenceLandmarks
      .map(refLm => (refLm, targetLandmarks.find(targetLm => targetLm.id == refLm.id)))
      .filter(lmTuple => lmTuple._2.nonEmpty)
      .map(lmTuple => LandmarkPair(lmTuple._1, lmTuple._2.get))
  }

  def main(args: Array[String]): Unit = {

    scalismo.initialize()

    val dataProvider = LOrealDataProvider

    val meshes = for (expression <- dataProvider.Expressions.expressionList()) {

      val referenceMesh = dataProvider.registration.loadPriorModel(expression).get.referenceMesh
      val referenceLandmarks = dataProvider.incoming.reference.loadLandmarks(expression).get

      val exp = for (id <- dataProvider.registration.ids(expression).filter(p => dataProvider.BlackList.blackList().forall(_.id != p.id)).par) yield {
        val targetLandmarks = dataProvider.incoming.loadLandmarks(id,expression).get
        val correspondingLandmarks = correspondingLandmarkPairs(referenceLandmarks, targetLandmarks)
        val correspondingLandmarkPoints = correspondingLandmarks.map(lmPair => (lmPair.targetLandmark.point, lmPair.referenceLandmark.point))
        val alignmentTransform = LandmarkRegistration.similarity3DLandmarkRegistration(correspondingLandmarkPoints, center = Point(0.0, 0.0, 0.0))
        dataProvider.registration.loadMesh(id,expression).get.transform(alignmentTransform)
      }

      val collection = DataCollection.fromMeshSequence(referenceMesh,exp.toIndexedSeq)._1.get
      val gpa = DataCollection.gpa(collection)
      val ssm = StatisticalMeshModel.createUsingPCA(gpa).get
      println(s"SSM ${expression.toString}")
      StatismoIO.writeStatismoMeshModel(ssm,new File(dataProvider.repositoryRoot.jfile,s"model${expression.toString}.h5"))

    }

  }

}
