/*
 * Copyright 2016 University of Basel, Graphics and Vision Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package modelbuilding.textureExtraction

import scalismo.faces.color.RGBA
import scalismo.faces.image.{PixelImage, PixelImageDomain}

import scala.reflect.ClassTag

object ImageScalingOperations {

  def maxPoolingPyramid(image: PixelImage[Double]): Seq[PixelImage[Double]] = buildPyramid(image, maxPoolingDownsampling)

  def pushPullPyramid(image: PixelImage[RGBA]): Seq[PixelImage[RGBA]] = buildPyramid(image, filteredAveragedDownsampling)

  def filteredAveragePyramid(image: PixelImage[RGBA], mask: PixelImage[Double]): Seq[(PixelImage[RGBA], PixelImage[Double])] = buildMaskedPyramid(image, mask, filteredAveragedDownsampling)

  def upsampling(image: PixelImage[RGBA]): PixelImage[RGBA] = {
    PixelImage(image.width * 2, image.height * 2, (x: Int, y: Int) => image((x / 2.0).floor.toInt, (y / 2.0).floor.toInt))
  }

  def buildPyramid[A: ClassTag](
    image: PixelImage[A],
    downsampling: PixelImage[A] => PixelImage[A],
    levels: Int = -1
  ): Seq[PixelImage[A]] = {

    if (image.width == 1 || image.height == 1 || levels == 0) image +: Nil
    else {
      val imageDown = downsampling(image)
      image +: buildPyramid(imageDown, downsampling, levels-1)
    }

  }

  def buildMaskedPyramid[A: ClassTag](
    image: PixelImage[A],
    mask: PixelImage[Double],
    downsampling: (PixelImage[A], PixelImage[Double]) => (PixelImage[A], PixelImage[Double]),
    levels: Int = -1
  ): Seq[(PixelImage[A], PixelImage[Double])] = {

    if (image.width == 1 || image.height == 1 || levels == 0) (image, mask) +: Nil
    else {
      val (imageDown, maskDown) = downsampling(image, mask)
      (image, mask) +: buildMaskedPyramid(imageDown, maskDown, downsampling, levels -1)
    }

  }

  def maxPoolingDownsampling(image: PixelImage[Double]): PixelImage[Double] = {
    val id = PixelImageDomain(image.width / 2, image.height / 2)
    val imageBuffer = PixelImage(id, (_, _) => 0.0).toBuffer
    for (x <- (0 until id.width).par; y <- 0 until id.height) {
      var color = 0.0
      for (xx <- 2 * x to (2 * x + 1); yy <- 2 * y to (2 * y + 1)) {
        if (image(xx, yy) > color) {
          color = image(xx, yy)
        }
      }
      imageBuffer(x, y) = color
    }
    imageBuffer.toImage
  }

  def filteredAveragedDownsampling(image: PixelImage[RGBA], mask: PixelImage[Double]): (PixelImage[RGBA], PixelImage[Double]) = {
    val id = PixelImageDomain(image.width / 2, image.height / 2)
    val imageBuffer = PixelImage(id, (_, _) => RGBA.BlackTransparent).toBuffer
    val maskBuffer = PixelImage(id, (_, _) => 0.0).toBuffer
    for (
      x <- (0 until id.width).par;
      y <- 0 until id.height
    ) {
      var color = RGBA.BlackTransparent
      var value = 0.0
      var counter = 0
      for (
        xx <- 2 * x to (2 * x + 1);
        yy <- 2 * y to (2 * y + 1) if image(xx, yy) != RGBA.BlackTransparent
      ) {
        counter += 1
        color += image(xx, yy)
        value += mask(xx, yy)
      }
      imageBuffer(x, y) = if (counter == 0) RGBA.BlackTransparent else color / counter
      maskBuffer(x, y) = if (counter == 0) 0.0 else value / counter
    }
    (imageBuffer.toImage, maskBuffer.toImage)
  }

  def filteredAveragedDownsampling(image: PixelImage[RGBA]): PixelImage[RGBA] = {
    val id = PixelImageDomain(image.width / 2, image.height / 2)
    val imageBuffer = PixelImage(id, (_, _) => RGBA.BlackTransparent).toBuffer
    for (
      x <- (0 until id.width).par;
      y <- 0 until id.height
    ) {
      var color = RGBA.BlackTransparent
      var counter = 0
      for (
        xx <- 2 * x to (2 * x + 1);
        yy <- 2 * y to (2 * y + 1) if image(xx, yy) != RGBA.BlackTransparent
      ) {
        counter += 1
        color += image(xx, yy)
      }
      imageBuffer(x, y) = if (counter == 0) RGBA.BlackTransparent else color / counter
    }
    imageBuffer.toImage
  }

  def buildPushPullPyramid(image: PixelImage[RGBA]): Seq[PixelImage[RGBA]] = {
    if (image.width <= 1 || image.height <= 1) image +: Nil
    else {
      val id = PixelImageDomain(image.width / 2, image.height / 2)
      val imageBuffer = PixelImage(id, (_, _) => RGBA.BlackTransparent).toBuffer
      for (x <- (0 until id.width).par; y <- 0 until id.height) {
        var color = RGBA.BlackTransparent
        var counter = 0
        for (xx <- 2 * x to (2 * x + 1); yy <- 2 * y to (2 * y + 1)) {
          if (image(xx, yy) != RGBA.BlackTransparent) {
            counter += 1
            color += image(xx, yy)
          }
        }
        imageBuffer(x, y) = if (counter == 0) RGBA.BlackTransparent else color / counter
      }
      image +: buildPushPullPyramid(imageBuffer.toImage)
    }
  }

//  def reducePushPullPyramid(seq: Seq[PixelImage[RGBA]]): PixelImage[RGBA] = {
//    seq.reverse.foldLeft(seq.last) { (lower: PixelImage[RGBA], higher: PixelImage[RGBA]) =>
//      val filled = PixelImage(higher.domain, (_, _) => RGBA.BlackTransparent).toBuffer
//      val l = lower.withAccessMode(Repeat())
//      for (x <- 0 until higher.width; y <- 0 until higher.height) {
//        val col = higher(x, y)
//        val dx = if (x % 2 == 0) -1 else 1
//        val dy = if (y % 2 == 0) -1 else 1
//        val altX = x / 2
//        val altY = y / 2
//        val dd = (0, 0) +: (dx, 0) +: (0, dy) +: (dx, dy) +: Nil
//        val ws = 9.0 +: 3.0 +: 3.0 +: 1.0 +: Nil
//        val W = ws.sum
//        val a = dd.
//          map(d => (altX + d._1, altY + d._2)).
//          filter(d => d._1 >= 0 || d._2 >= 0 || d._1 < l.width || d._2 < l.height).
//          zip(ws).
//          map(p => (l(p._1._1, p._1._2) * p._2, p._2)).
//          foldLeft((RGBA(0, 0, 0, 0), 0.0))((a, b) => (a._1 + b._1, a._2 + b._2))
//        val alt = a._1 / a._2
//        filled(x, y) = if (col != RGBA.BlackTransparent) col else alt
//      }
//      filled.toImage
//    }
//  }

  def reducePPPyramid(seq: Seq[PixelImage[RGBA]]): PixelImage[RGBA] = {
    seq.reverse.tail.foldLeft(seq.last)((a,b) => fillUpperLevel(a,b))
  }

  def fillUpperLevel(lower: PixelImage[RGBA], higher: PixelImage[RGBA]): PixelImage[RGBA] = {
    assert(higher.width == lower.width*2, s"higher.width:${higher.width} != 2xlower.width:${lower.width}")
    assert(higher.height == lower.height*2, s"higher.height:${higher.height} != 2xlower.height:${lower.height}")
    val filled = PixelImage(higher.domain, (_, _) => RGBA.BlackTransparent).toBuffer
    for (x <- 0 until higher.width; y <- 0 until higher.height) {
      val X = (x/2.0).floor.toInt
      val Y = (y/2.0).floor.toInt
      val alt = lower(X,Y)
      val col = higher(x, y)
      filled(x, y) = if (col != RGBA.BlackTransparent) col else alt
    }
    filled.toImage
  }

}


