/*
 * Copyright 2016 University of Basel, Graphics and Vision Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package modelbuilding.textureExtraction

import faces.mesh.TextureMappedProperty
import scalismo.faces.image.{ImageBuffer, PixelImage, PixelImageDomain}
import scalismo.geometry.{Point, Vector, _2D, _3D}
import scalismo.mesh.{BarycentricCoordinates, MeshSurfaceProperty, TriangleId}

object TextureMappingUtils {

  def calculateBarycentricTextureImage(
                                        textureDomain: PixelImageDomain,
                                        mapping: MeshSurfaceProperty[Point[_2D]]
                                      ): PixelImage[(Int, Vector[_3D])] = {
    val w = textureDomain.width
    val h = textureDomain.height
    val barycentricBuffer = ImageBuffer[(Int, Vector[_3D])](textureDomain, Array.fill(w * h)(0, Vector(0.0, 0.0, 0.0)))

    for (t <- mapping.triangulation.triangles.indices.par) {
      val tid = TriangleId(t)
      val a = TextureMappedProperty.imageInterpolatedCoordinatesFromUV(mapping.onSurface(tid, BarycentricCoordinates(1.0, 0.0, 0.0)), w - 1, h - 1)
      val b = TextureMappedProperty.imageInterpolatedCoordinatesFromUV(mapping.onSurface(tid, BarycentricCoordinates(0.0, 1.0, 0.0)), w - 1, h - 1)
      val c = TextureMappedProperty.imageInterpolatedCoordinatesFromUV(mapping.onSurface(tid, BarycentricCoordinates(0.0, 0.0, 1.0)), w - 1, h - 1)
      rasterTextureTriangle(tid, a, b, c, (tid.id, Vector(1.0, 0.0, 0.0)), (tid.id, Vector(0.0, 1.0, 0.0)), (tid.id, Vector(0.0, 0.0, 1.0)), barycentricBuffer)
    }

    barycentricBuffer.toImage
  }

  /**
    * rasterize a single triangle
    *
    * @param triangleId triangle id in triangulation
    * @param a          point A in triangle
    * @param b          point B in triangle
    * @param c          point C in triangle
    * @param buffer     render buffer, stores the painted values and does depth management
    */
  def rasterTextureTriangle(
                             triangleId: TriangleId,
                             a: Point[_2D],
                             b: Point[_2D],
                             c: Point[_2D],
                             av: (Int, Vector[_3D]),
                             bv: (Int, Vector[_3D]),
                             cv: (Int, Vector[_3D]),
                             buffer: ImageBuffer[(Int, Vector[_3D])]
                           ) = {

    // bounding box
    val minX = math.min(a.x, math.min(b.x, c.x)).floor.toInt
    val maxX = math.max(a.x, math.max(b.x, c.x)).ceil.toInt
    val minY = math.min(a.y, math.min(b.y, c.y)).floor.toInt
    val maxY = math.max(a.y, math.max(b.y, c.y)).ceil.toInt

    // determine winding order
    val signedArea = (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x)
    val ccwWinding = signedArea > 0

    // point inside? careful with ccw/cw windings, shader decides what to do in each case
    @inline
    def inTriangle(x: Int, y: Int): Boolean = {
      val xf = x.toDouble
      val yf = y.toDouble
      // all "left of line"? or all "right of line"?
      if (ccwWinding)
        (b.x - a.x) * (yf - a.y) - (b.y - a.y) * (xf - a.x) >= 0 &&
          (c.x - b.x) * (yf - b.y) - (c.y - b.y) * (xf - b.x) >= 0 &&
          (a.x - c.x) * (yf - c.y) - (a.y - c.y) * (xf - c.x) >= 0
      else
        (b.x - a.x) * (yf - a.y) - (b.y - a.y) * (xf - a.x) <= 0 &&
          (c.x - b.x) * (yf - b.y) - (c.y - b.y) * (xf - b.x) <= 0 &&
          (a.x - c.x) * (yf - c.y) - (a.y - c.y) * (xf - c.x) <= 0
    }

    def isDefinedAt(x: Int, y: Int): Boolean = {
      0 <= x && x < buffer.width &&
        0 <= y && y < buffer.height
    }

    // inner loop over pixels in bounding box
    var y = minY
    while (y <= maxY) {
      var x = minX
      while (x <= maxX) {
        if (inTriangle(x, y) && isDefinedAt(x, y)) {
          val screenBCC = BarycentricCoordinates.pointInTriangle(Point(x, y), Point(a.x, a.y), Point(b.x, b.y), Point(c.x, c.y))
          buffer(x, y) = (av._1, av._2 * screenBCC.a + bv._2 * screenBCC.b + cv._2 * screenBCC.c)
        }
        x += 1
      }
      y += 1
    }
  }
}
