/*
 * Copyright 2016 University of Basel, Graphics and Vision Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package modelbuilding.textureExtraction

import faces.sampling.face.evaluators.PointEvaluators.IsotropicGaussianPointEvaluator
import scalismo.faces.color.ColorBlender
import scalismo.faces.image._
import scalismo.geometry.{Point1D, _1D}

import scala.math._

/**
  * Operations on masks, i.e. PixelImage[Double] with values between [0.0,1.0]
  */
object MaskManipulations {

  /**
    * Erode smoothly the values towards zero while values further away than dist from a border do not change.
    * The method is based on the GeneralMaxConvolution.
    */
  def erodeSmoothly(mask: PixelImage[Double], dist: Double = 2.0): PixelImage[Double] = {
    val inverted = mask.map(e => log(1.0-e))
    val eval = IsotropicGaussianPointEvaluator[_1D](dist).toDistributionEvaluator(Point1D(0.0))
    val smoothed = faces.utils.GeneralMaxConvolution.separableMaxConvolution(inverted, eval)
    smoothed.map{ f =>
      1.0 - exp(f-2*eval.logValue(Point1D(0.0)))
    }
  }

  /**
    * Averages over several successive applications of minimumAccountingAlsoFor8Neighbourhood.
    */
  def erodeLinearUsing8Neighbourhood(mask: PixelImage[Double], level: Int): PixelImage[Double] = {
    if ( level == 0 ) mask
    else {
      val errode = minimumAccountingAlsoFor8Neighbourhood(mask)

      val lower = erodeLinearUsing8Neighbourhood(errode, level - 1)
      PixelImage(mask.domain, (x,y) => ColorBlender.doubleBlender.blend(mask(x,y), lower(x,y), 1.0 / (1.0 + level)))
    }
  }

  /**
    * This function sets each pixel to the minimum of itself and the 8-neighbourhood.
    */
  def minimumAccountingAlsoFor8Neighbourhood(mask: PixelImage[Double]): PixelImage[Double] = {
    val padded = mask.withAccessMode(AccessMode.Padded(1.0))
    val deltas = Seq((0,0),(-1,-1),(0,-1),(-1,0),(1,-1),(-1,1),(1,0),(0,1),(1,1))

    PixelImage(mask.domain,(X,Y) => {
      deltas.map(d => padded(X+d._1,Y+d._2)).min
    })
  }

}

