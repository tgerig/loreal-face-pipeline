/*
 * Copyright 2016 University of Basel, Graphics and Vision Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package modelbuilding.textureExtraction

import java.io.File

import modelbuilding.pyramid.{GaussPyramid, ImagePyramid}
import scalismo.faces.color.{ColorBlender, ColorSpaceOperations, RGBA}
import scalismo.faces.image._
import scalismo.faces.image.filter.ImageFilter
import scalismo.faces.image.PixelImage.implicits._

import scala.util.Random


class PushPullPyramid(val differencePyramid: ImagePyramid[RGBA],
                      val expand: ImageFilter[RGBA, RGBA]
                     ) {
  val levels = differencePyramid.levels
  val level = differencePyramid.level

  val id = Random.nextInt(1000)

  val reconstruct = level.init.zipWithIndex.foldRight(level.last)((diff, combined) => {
    val upSampled = expand(combined)
    implicit val ops = PushPullPyramid.addOps
    val combi = diff._1 + upSampled
    combi
  })

}


object PushPullPyramid {



  /**
    * This is used by the gauss & laplace pyramid as border condition.
    * Usually this is used with the combine function: (a,b) => 2*a-b.
    *
    * @param combine Functional to combine the two values at positions.
    */
  case class MirroredPositionFunctional[A](combine: (A,A) => A) extends AccessMode[A] {
    override def map[B](f: (A) => B): AccessMode[B] = ???

    override def outsideAccess(x: Int, y: Int, image: PixelImage[A]): A = {
      if (image.domain.isDefinedAt(x, y))
        image(x, y)
      else {
        val dx = if (x < 0) Some((0, clamp(x * (-1),0,image.width-1))) else if (x >= image.width) Some((image.width - 1, clamp(2 * (image.width - 1) - x,0,image.width-1))) else None
        val dy = if (y < 0) Some((0, clamp(y * (-1),0,image.height-1))) else if (y >= image.height) Some((image.height - 1, clamp(2 * (image.height - 1) - y,0,image.height-1))) else None
        if (dx.isDefined && dy.isDefined) {
          val px = dx.get
          val py = dy.get
          combine(image.valueAt(px._1, py._1), image.valueAt(px._2, py._2))
        } else if (dx.isDefined) {
          val px = dx.get
          combine(image.valueAt(px._1, y), image.valueAt(px._2, y))
        } else if (dy.isDefined) {
          val py = dy.get
          combine(image.valueAt(x, py._1),image.valueAt(x, py._2))
        } else {
          throw new Exception(s"image access mode was outside but now inside.... confusing: ($x, $y), size=(${image.width}, ${image.height})")
        }
      }
    }

    private def clamp(i: Int, min: Int, max: Int): Int = math.min(max, math.max(min, i))
  }


  def apply(image: PixelImage[RGBA], reductions: Int = -1)(implicit ops: ColorSpaceOperations[RGBA]): PushPullPyramid = {
    val imagePyramid = new GaussPyramid[RGBA](image, reduce, reductions)
//    val differencePyramid = new LaplacePyramid[RGBA](imagePyramid, expand)(classTag[RGBA], diffOps)

    val differencePyramid = new ImagePyramid[RGBA] {
      var upsampled = imagePyramid.level.last

      override val levels: Int = imagePyramid.levels
      override val level: Seq[PixelImage[RGBA]] = imagePyramid.level.init.reverse.map{ img: PixelImage[RGBA] =>
        upsampled = expand(upsampled)
        implicit val ops = diffOps
        import PixelImage.implicits._
        img - upsampled
      }.reverse :+ imagePyramid.level.last
    }

    imagePyramid.level.indices.foreach(i => PixelImageIO.write(imagePyramid.level(i),new File(s"/tmp/maskOperations/gauss${i}.png")))
    differencePyramid.level.indices.foreach(i => PixelImageIO.write(differencePyramid.level(i).map(_*0.5+RGBA(0.5)),new File(s"/tmp/maskOperations/laplace${i}.png")))

    new PushPullPyramid(differencePyramid, expand)
  }

  def reduce(implicit ops: ColorSpaceOperations[RGBA]) = new ImageFilter[RGBA, RGBA] {
    override def filter(img: PixelImage[RGBA]) = {
      val w = img.width / 2
      val h = img.height / 2
      val deltas = Seq(0, 1)
      PixelImage[RGBA](w, h, (x: Int, y: Int) => {
        val values = deltas.flatMap { dx =>
          deltas.map { dy =>
            val col: RGBA = img(2 * x + dx, 2 * y + dy)
            (col, col.a)
          }
        }
        ColorBlender.fromColorSpace[RGBA](ops).convexCombination(values.head, values.tail: _*)
      })
    }
  }

  def expand = new ImageFilter[RGBA, RGBA] {
    override def filter(img: PixelImage[RGBA]): PixelImage[RGBA] = {
      val w = img.width * 2
      val h = img.height * 2

//      PixelImage(w,h,(x:Int,y:Int) => img((x/2.0).floor.toInt,(y/2.0).floor.toInt))

      val kernel = InterpolationKernel.CubicKernel.mitchellNetravali
      resampleAlphaWeightedImage(img.withAccessMode(MirroredPositionFunctional((a:RGBA,b:RGBA)=>2*:a-b)), w, h, kernel)
    }
  }

  private[textureExtraction] def resampleAlphaWeightedImage(image: PixelImage[RGBA],
                                                            cols: Int,
                                                            rows: Int,
                                                            kernel: InterpolationKernel)
                                                           (implicit ops: ColorSpaceOperations[RGBA]) = {
    require(cols > 0 && rows > 0, s"cannot resample to new size of 0: ($cols, $rows)")
    require(image.width > 0 && image.height > 0, s"cannot resample image size of 0: (${image.width}, ${image.height})")

    val width = image.width
    val height = image.height

    // scale factor
    val scaleW = width.toDouble / cols
    val scaleH = height.toDouble / rows

    // kernel is only scaled in decimation case
    val kScaleW = math.max(1.0, scaleW)
    val kScaleH = math.max(1.0, scaleH)

    // kernel
    val rx = kernel.radius * kScaleW
    val ry = kernel.radius * kScaleH

    // row filter for the image
    def filter(cols: Int, scale: Double, kScale: Double) = new ImageFilter[RGBA, RGBA] {
      def filter(image: PixelImage[RGBA]): PixelImage[RGBA] = {
        def f(i: Int, j: Int): RGBA = {
          // center point for access in this continuous image
          val x = i + 0.5
          // center point in underlying image grid (i,j)
          val ix = x * scale - 0.5
          // filtering, scaled kernel, spacing of original image
          val left = math.ceil(ix - rx).toInt
          val right = math.floor(ix + rx).toInt
          // do filtering
          var sx: Int = left
          var kSumX: Double = 0.0
          var kvsum: RGBA = RGBA.BlackTransparent
          while (sx <= right) {
            val col = image(sx, j)
            val k = kernel((ix - sx) / kScale)
            kSumX += k
            kvsum += col * k
            //            if ( col.a == 1.0 ) {
            //              val k = kernel((ix - sx) / kScale)
            //              kSumX += k
            //              kvsum += col * k
            //            }
            sx += 1
          }
          if (kSumX == 0.0) ops.zero else kvsum / kSumX
        }
        PixelImage(cols, image.height, f)
      }
    }

    val rowFilter = filter(cols, scaleW, kScaleW)
    val colFilter = filter(rows, scaleH, kScaleH)

    // do the resampling: choose inner loop based on image domain, buffer the intermediate result (careful with access modes)
    image.domain match {
      case d: ColumnMajorImageDomain => image.transposed.filter(colFilter).transposed.filter(rowFilter)
      case d: RowMajorImageDomain => image.filter(rowFilter).transposed.filter(colFilter).transposed
    }
  }

  private[textureExtraction] val addOps = new ColorSpaceOperations[RGBA] {
    /// add two pixels
    override def add(pix1: RGBA, pix2: RGBA): RGBA = {
      if (pix1.a == 0.0) if(pix2.a == 0 ) zero else pix2
      else if (pix2.a == 0.0) if(pix1.a == 0 ) zero else pix1
      else RGBA(pix1.r + pix2.r, pix1.g + pix2.g, pix1.b + pix2.b, 1.0)
    }

    /// channel-wise multiplication
    override def multiply(pix1: RGBA, pix2: RGBA): RGBA = pix1.x(pix2)

    /// dot product
    override def dot(pix1: RGBA, pix2: RGBA): Double = pix1.dot(pix2)

    override def dimensionality: Int = 4

    /// scalar multiplication
    override def scale(pix: RGBA, l: Double): RGBA = RGBA(pix.r * l, pix.g * l, pix.b * l, pix.a)

    /// zero element
    override def zero: RGBA = RGBA.BlackTransparent
  }

  private[textureExtraction] val diffOps = new ColorSpaceOperations[RGBA] {
    /// add two pixels
    override def add(pix1: RGBA, pix2: RGBA): RGBA = {
      if (pix1.a == 0.0) zero
      else if (pix2.a == 0.0) zero
      else RGBA(pix1.r + pix2.r, pix1.g + pix2.g, pix1.b + pix2.b, 1.0)
    }

    /// channel-wise multiplication
    override def multiply(pix1: RGBA, pix2: RGBA): RGBA = pix1.x(pix2)

    /// dot product
    override def dot(pix1: RGBA, pix2: RGBA): Double = pix1.dot(pix2)

    override def dimensionality: Int = 4

    /// scalar multiplication
    override def scale(pix: RGBA, l: Double): RGBA = RGBA(pix.r * l, pix.g * l, pix.b * l, pix.a)

    /// zero element
    override def zero: RGBA = RGBA.BlackTransparent
  }
}
