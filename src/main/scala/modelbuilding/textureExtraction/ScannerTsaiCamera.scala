/*
 * Copyright 2017 University of Basel, Graphics and Vision Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package modelbuilding.textureExtraction

import faces.parameters.Camera
import faces.render.{PointShader, RenderTransforms, Rotation3D, _}
import scalismo.geometry._

import scala.collection.mutable.ListBuffer
import scala.io.Source


class ScannerTsaiParameters(val cameraParameters: CameraParameters,
                            val calibrationParameters: CalibrationParameters,
                            val textureRecallParameters: TextureRecallParameters,
                            val additionalTransformationParameters: AdditionalTransformationParameters) {

  override def toString: String = {
    "Tsai{ %s, %s, %s, %s }".format(
      cameraParameters, calibrationParameters, textureRecallParameters, additionalTransformationParameters
    )
  }

  def camera = Camera(
    focalLength = cal.f,
    principalPoint = principalPoint,
    sensorSize = pixelSpacing,
    near = dummyNear,
    far = dummyFar,
    orthographic = orthographic
  )

  def tsaiCamera: TsaiCamera = {
    TsaiCamera(
      translation,
      pitch,
      yaw,
      roll,
      focalLength,
      dummyNear,
      dummyFar,
      distortion,
      principalPoint,
      pixelSpacing
    )
  }

  //maps the C++ style rotation to the rotation used in this framework
  private case class fromScalatoCYRotation(phi: Double) extends InvertibleTransform3D {
    /** inverted version of this transform */
    override def inverted: InvertibleTransform3D = fromScalatoCYRotation(-phi)

    /** apply transform to a 3d point */
    override def apply(x: Point[_3D]): Point[_3D] = Point(-2.0 * x.z * math.sin(phi), 0.0, 2.0 * x.x * math.sin(phi))

    /** apply transform to a 3d vector */
    override def apply(v: Vector[_3D]): Vector[_3D] = Vector(-2.0 * v.z * math.sin(phi), 0.0, 2.0 * v.x * math.sin(phi))

    val rotationMatrix: SquareMatrix[_3D] = {
      SquareMatrix((0.0, 0.0, -2 * math.sin(phi)), (0.0, 0.0, 0.0), (2 * math.sin(phi), 0.0, 0.0))
    }
  }

  val cam = cameraParameters
  val cal = calibrationParameters
  val trp = textureRecallParameters
  val atp = additionalTransformationParameters

  val orthographic = false

  val scaleFactor = trp.scale
  val distortion = cal.kappa1
  val focalLength = cal.f

  val dummyFar = 1e5
  val dummyNear = 1.0

  val pixelSpacing = Vector(cam.dx / scaleFactor, cam.dy)

  val principalPoint = (SquareMatrix((Math.cos(trp.angle), -Math.sin(trp.angle)),
    (Math.sin(trp.angle), Math.cos(trp.angle))
  ) * Vector(cam.Cx * trp.scale, cam.Cy) + Vector(trp.tx, trp.ty)).toPoint

  val rotationMatrix = Rotation3D.rotationZ(trp.angle).rotationMatrix

  val rotationToCamera = Rotation3D.rotationZ(cal.Rz).rotationMatrix *
    (fromScalatoCYRotation(-cal.Ry).rotationMatrix + Rotation3D.rotationY(-cal.Ry).rotationMatrix) *
    Rotation3D.rotationX(cal.Rx).rotationMatrix

  val correctionMatrix = Rotation3D.rotationZ(-atp.rz).rotationMatrix *
    (fromScalatoCYRotation(atp.ry).rotationMatrix + Rotation3D.rotationY(atp.ry).rotationMatrix) *
    Rotation3D.rotationX(-atp.rx).rotationMatrix

  val full_matrix = rotationMatrix * rotationToCamera * correctionMatrix
  val roll = math.atan2(full_matrix(1, 0), full_matrix(0, 0))
  val yaw = math.atan2(-full_matrix(2, 0), math.sqrt(math.pow(full_matrix(2, 1), 2) + math.pow(full_matrix(2, 2), 2)))
  val pitch = math.atan2(full_matrix(2, 1), full_matrix(2, 2))

  val translation = rotationMatrix * ((rotationToCamera.*(-1) * (correctionMatrix * Vector(atp.tx, atp.ty, atp.tz))) + Vector(cal.Tx, cal.Ty, cal.Tz))

}

case class CameraParameters(Ncx: Int,
                            Nfx: Int,
                            dx: Double,
                            dy: Double,
                            dpx: Double,
                            dpy: Double,
                            Cx: Double,
                            Cy: Double,
                            sx: Double) {
  override def toString: String = {
    "CameraParameters{ Ncx(%d), Nfx(%d), dx(%f), dy(%f), dpx(%f), dpy(%f), Cx(%f), Cy(%f), sx(%f) }".format(
      Ncx, Nfx, dx, dy, dpx, dpy, Cx, Cy, sx
    )
  }
}

case class CalibrationParameters(f: Double,
                                 kappa1: Double,
                                 p1: Double,
                                 p2: Double,
                                 Tx: Double,
                                 Ty: Double,
                                 Tz: Double,
                                 Rx: Double,
                                 Ry: Double,
                                 Rz: Double,
                                 r1: Double,
                                 r2: Double,
                                 r3: Double,
                                 r4: Double,
                                 r5: Double,
                                 r6: Double,
                                 r7: Double,
                                 r8: Double,
                                 r9: Double) {
  override def toString: String = {
    "CalibrationParameters{ f(%f), kappa1(%f), p1(%f), p2(%f), Tx(%f), Ty(%f), Tz(%f), Rx(%f), Ry(%f), Rz(%f), r1(%f), r2(%f), r3(%f), r4(%f), r5(%f), r6(%f), r7(%f), r8(%f), r9(%f) }".format(
      f, kappa1, p1, p2, Tx, Ty, Tz, Rx, Ry, Rz, r1, r2, r3, r4, r5, r6, r7, r8, r9
    )
  }
}

case class TextureRecallParameters(recalibrated: Boolean,
                                   angle: Double,
                                   scale: Double,
                                   tx: Double,
                                   ty: Double,
                                   M00: Double,
                                   M01: Double,
                                   M10: Double,
                                   M11: Double) {
  override def toString: String = {
    "TextureRecallParameters{ recalibrated(%d), angle(%f), scale(%f), tx(%f), ty(%f), M00(%f), M01(%f), M10(%f), M11(%f) }".format(
      if (recalibrated) 1 else 0, angle, scale, tx, ty, M00, M01, M10, M11
    )
  }
}

case class AdditionalTransformationParameters(tx: Double,
                                              ty: Double,
                                              tz: Double,
                                              rx: Double,
                                              ry: Double,
                                              rz: Double,
                                              m00: Double,
                                              m01: Double,
                                              m02: Double,
                                              m10: Double,
                                              m11: Double,
                                              m12: Double,
                                              m20: Double,
                                              m21: Double,
                                              m22: Double) {
  override def toString: String = {
    "AdditionalTranformationParameters{ tx(%f), ty(%f), tz(%f), rx(%f), ry(%f), rz(%f), m00(%f), m01(%f), m02(%f), m10(%f), m11(%f), m12(%f), m20(%f), m21(%f), m22(%f) }".format(
      tx, ty, tz, rx, ry, rz, m00, m01, m02, m10, m11, m12, m20, m21, m22
    )
  }
}

object ScannerTsaiParameters {

  def fromFile(filename: String): ScannerTsaiParameters = ScannerTsaiParameters.fromSource(Source.fromFile(filename))

  def fromSource(is: Source): ScannerTsaiParameters = ScannerTsaiParameters.fromStringList(calStreamParser(is))

  def fromStringList(strings: List[String]): ScannerTsaiParameters = {
    require(strings.length == 52)
    new ScannerTsaiParameters(
      createCameraParameters(strings),
      createCalibrationParameters(strings),
      createTextureRecallParameters(strings),
      createAdditionalTransformationParameters(strings)
    )
  }

  private def calStreamParser(is: Source): List[String] = {
    val string_list = ListBuffer[String]()
    for (line <- is.getLines()) {
      if (!line.contains("[")) {
        val split_string = line.split("=").toList
        if (split_string.size > 1) {
          string_list += split_string.last
        }
      }
    }
    string_list.toList
  }

  private def createCameraParameters(strings: List[String]): CameraParameters = {
    CameraParameters(
      Ncx = strings.head.toInt,
      Nfx = strings(1).toInt,
      dx = strings(2).toDouble,
      dy = strings(3).toDouble,
      dpx = strings(4).toDouble,
      dpy = strings(5).toDouble,
      Cx = strings(6).toDouble,
      Cy = strings(7).toDouble,
      sx = strings(8).toDouble
    )
  }

  private def createCalibrationParameters(stringList: List[String]): CalibrationParameters = {
    CalibrationParameters(
      f = stringList(9).toDouble,
      kappa1 = stringList(10).toDouble,
      p1 = stringList(11).toDouble,
      p2 = stringList(12).toDouble,
      Tx = stringList(13).toDouble,
      Ty = stringList(14).toDouble,
      Tz = stringList(15).toDouble,
      Rx = stringList(16).toDouble,
      Ry = stringList(17).toDouble,
      Rz = stringList(18).toDouble,
      r1 = stringList(19).toDouble,
      r2 = stringList(20).toDouble,
      r3 = stringList(21).toDouble,
      r4 = stringList(22).toDouble,
      r5 = stringList(23).toDouble,
      r6 = stringList(24).toDouble,
      r7 = stringList(25).toDouble,
      r8 = stringList(26).toDouble,
      r9 = stringList(27).toDouble
    )
  }

  private def createTextureRecallParameters(stringList: List[String]): TextureRecallParameters = {
    TextureRecallParameters(
      recalibrated = stringList(28).toBoolean,
      angle = stringList(29).toDouble,
      scale = stringList(30).toDouble,
      tx = stringList(31).toDouble,
      ty = stringList(32).toDouble,
      M00 = stringList(33).toDouble,
      M01 = stringList(34).toDouble,
      M10 = stringList(35).toDouble,
      M11 = stringList(36).toDouble)
  }

  private def createAdditionalTransformationParameters(stringList: List[String]): AdditionalTransformationParameters = {
    AdditionalTransformationParameters(
      tx = stringList(37).toDouble,
      ty = stringList(38).toDouble,
      tz = stringList(39).toDouble,
      rx = stringList(40).toDouble,
      ry = stringList(41).toDouble,
      rz = stringList(42).toDouble,
      m00 = stringList(43).toDouble,
      m01 = stringList(44).toDouble,
      m02 = stringList(45).toDouble,
      m10 = stringList(46).toDouble,
      m11 = stringList(47).toDouble,
      m12 = stringList(48).toDouble,
      m20 = stringList(49).toDouble,
      m21 = stringList(50).toDouble,
      m22 = stringList(51).toDouble
    )
  }

}


case class TsaiCamera(translation: Vector[_3D],
                      pitch: Double,
                      yaw: Double,
                      roll: Double,
                      focalLength: Double,
                      near: Double,
                      far:Double,
                      distortion: Double,
                      principalPoint: Point[_2D],
                      pixelSpacing: Vector[_2D]) {

  val cameraTransform = RenderTransforms.viewTransform(translation, pitch, yaw, roll)
  def getOrigin = cameraTransform(-translation).toPoint

  def worldToCamera(point: Point[_3D]) = cameraTransform.inverted(point)
  def worldFromCamera(point: Point[_3D]) = cameraTransform(point)

  def cameraToPlane(point: Point[_3D]) = Point(
    x = focalLength * point.x / point.z,
    y = focalLength * point.y / point.z,
    z = point.z
  )


  def cameraFromPlane(point: Point[_2D]) = Point(point.x, point.y, focalLength)

  def planeToDistortedPlane(point: Point[_3D]): Point[_3D] = {
    def to2d(p: Point[_3D]): Point[_2D] = Point(p.x, p.y)
    val point2d = to2d(point)

    if (point2d.toVector.norm2 > 0.0) {

      val rru = point2d.toVector.norm
      val c = 1.0 / distortion
      val d = -c * rru
      val qq = c / 3.0
      val rr = -d / 2.0
      val dd = math.pow(qq, 3.0) + Math.pow(rr, 2.0)
      val lambda = if (dd >= 0.0) {
        val sD = math.sqrt(dd)
        val ss = math.cbrt(rr + sD)
        val t = math.cbrt(rr - sD)
        val rrd = ss + t
        rrd / rru
      } else {
        val sD = math.sqrt(-dd)
        val ss = math.cbrt(math.hypot(rr, sD))
        val t = math.atan2(sD, rr) / 3.0
        val sinT = math.sin(t)
        val cosT = math.cos(t)

        val sqrt3 = 1.732050807568877293527446341505872366943
        val rrd = -ss * cosT + sqrt3 * ss * sinT
        rrd / rru
      }
      //      val helper = (Vector(point2d.x, point2d.y, 0.0) * lambda).toPoint
      //      Point(helper.x, helper.y, point.z)

      Point(point2d.x * lambda, point2d.y * lambda, point.z)
    } else {
      point
    }
  }

  def planeFromDistortedPlane(point: Point[_2D]) = (point.toVector * (1.0 + distortion * point.toVector.norm2)).toPoint


  def distortedPlaneToImage(point: Point[_3D]) =
    Point(
      point.x / pixelSpacing.x + principalPoint.x,
      point.y / pixelSpacing.y + principalPoint.y,
      point.z)


  def distortedPlaneFromImage(point: Point[_2D]): Point[_2D] = Point(
    (point.x - principalPoint.x) * pixelSpacing.x,
    (point.y - principalPoint.y) * pixelSpacing.y
  )

  def worldToImage(point: Point[_3D]): Point[_3D] = distortedPlaneToImage(planeToDistortedPlane(cameraToPlane(worldToCamera(point))))
  def imageToWorld(point: Point[_2D]) = worldFromCamera(cameraFromPlane(planeFromDistortedPlane(distortedPlaneFromImage(point))))


  def pointShader(width: Int, height: Int): PointShader = new PointShader {
    val factorX = 2.0 / (pixelSpacing.x * width)
    val factorY = - 2.0 / (pixelSpacing.y * height)
    val factorZ = 2.0 / (far - near)
    val offsetZ = - (far + near) / (far - near)

    def distortedToNDC(point: Point[_3D], width: Int, height: Int): Point[_3D] = {
      Point(
        point.x * factorX,
        point.y * factorY,
        point.z * factorZ + offsetZ
      )
    }

    /** apply transform to a 3d point */
    override def apply(pt: Point[_3D]): Point[_3D] = {
      distortedToNDC(
        planeToDistortedPlane(
          cameraToPlane(
            worldToCamera(pt)
          )
        ), width, height
      )
    }

    /** apply transform to a 3d vector */
    def apply(vec: Vector[_3D]): Vector[_3D] = {
      distortedToNDC(
        planeToDistortedPlane(
          cameraToPlane(
            worldToCamera(vec.toPoint)
          )
        ), width, height
      ).toVector
    }
  }

  def windowTransform(width: Int, height: Int): InvertibleTransform3D = new InvertibleTransform3D {

    val halfWidth = width/2.0
    val negHalfHeight = -height/2.0

    /** inverted version of this transform */
    override def inverted: InvertibleTransform3D = ???

    /** apply transform to a 3d point */
    override def apply(point: Point[_3D]): Point[_3D] = {
      val pt = Point(
        point.x * halfWidth + principalPoint.x,
        point.y * negHalfHeight + principalPoint.y,
        point.z)
      pt
    }

    /** apply transform to a 3d vector */
    override def apply(vector: Vector[_3D]): Vector[_3D] = {
      Vector(
        vector.x * halfWidth + principalPoint.x,
        vector.y * negHalfHeight + principalPoint.y,
        vector.z)
    }
  }
}