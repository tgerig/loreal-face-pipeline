/*
 * Copyright 2016 University of Basel, Graphics and Vision Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package modelbuilding.textureExtraction

import modelbuilding.pyramid.{GaussPyramid, ImagePyramid}
import scalismo.faces.color.RGBA
import scalismo.faces.image.AccessMode.Repeat
import scalismo.faces.image.filter.IsotropicGaussianFilter
import scalismo.faces.image.{PixelImage, PixelImageDomain}

object TextureBlending {

  /**
   * Selects always the best visible texel.
   * The selection is based on the angular deviation of the surface normal to viewing ray of each camera.
   */
  def bestTexture(
    textures: Seq[PixelImage[RGBA]],
    normalViewRayDeviations: Seq[PixelImage[Double]]
  ): (PixelImage[RGBA], PixelImage[RGBA]) = {

    val textureDomain = textures.head.domain
    val bestUnique: Seq[PixelImage[Double]] = pixelwiseBestVisibleTexture(textureDomain, normalViewRayDeviations)

    val fusedTexture = new PixelImage[RGBA](textureDomain, Repeat[RGBA](), (_: Int, _: Int) => RGBA.BlackTransparent).toBuffer
    val weightImage = new PixelImage[RGBA](textureDomain, Repeat[RGBA](), (_: Int, _: Int) => RGBA.BlackTransparent).toBuffer
    for (xTd <- 0 until textureDomain.width; yTd <- 0 until textureDomain.height) {
      val col = (0 until 3).map { i => textures(i)(xTd, yTd) * bestUnique(i)(xTd, yTd) }.foldLeft(RGBA.BlackTransparent)((a, b) => a + b)
      weightImage(xTd, yTd) = RGBA(bestUnique(0)(xTd, yTd), bestUnique(1)(xTd, yTd), bestUnique(2)(xTd, yTd), 1)
      fusedTexture(xTd, yTd) = col
    }
    (fusedTexture.toImage, weightImage.toImage)
  }

  def blendTextures(
    textures: Seq[PixelImage[RGBA]],
    normalViewRayDeviations: Seq[PixelImage[Double]]
  ): (PixelImage[RGBA], PixelImage[RGBA]) = {

    val textureDomain = textures.head.domain

    val bestUnique: Seq[PixelImage[Double]] = pixelwiseBestVisibleTexture(textureDomain, normalViewRayDeviations)
    val bestSmoothed = bestUnique.map(_.withAccessMode(Repeat[Double]()).filter(IsotropicGaussianFilter(10.0)))

    val possible = textures.map(_.map(_.a))
    val possibleEroded = possible.map(mask => MaskManipulations.erodeLinearUsing8Neighbourhood(mask.withAccessMode(Repeat[Double]()), 10))

    val blendedTexture = new PixelImage[RGBA](textureDomain, Repeat[RGBA](), (_: Int, _: Int) => RGBA.BlackTransparent).toBuffer
    val weightBuffer = new PixelImage[RGBA](textureDomain, Repeat[RGBA](), (_: Int, _: Int) => RGBA.BlackTransparent).toBuffer

    for (xTd <- (0 until textureDomain.width).par; yTd <- 0 until textureDomain.height) {
      val color = textures.indices.map { (i: Int) =>
        {
          val col = textures(i)(xTd, yTd)
          val valid = (normalViewRayDeviations(i)(xTd, yTd) > -1.0) &&
            (possibleEroded(i)(xTd, yTd) > 0.0) &&
            (bestSmoothed(i)(xTd, yTd) > 0.0)
          if (valid) Some((col, normalViewRayDeviations(i)(xTd, yTd) * 0.5 + 0.5, possibleEroded(i)(xTd, yTd), bestSmoothed(i)(xTd, yTd)))
          else None
        }
      }
      blendedTexture(xTd, yTd) = {
        var mixedColor = RGBA.BlackTransparent
        var weights = 0.0
        var counter = 0

        val w = color.map {
          case Some(c: (RGBA, Double, Double, Double)) =>
            val col: RGBA = c._1
            val w: Double = c._2 * c._3 * c._4
            mixedColor += col * w
            weights += w
            counter += 1
            Some(w)
          case _ => None
        }
        val weighted = w.map {
          case Some(c: Double) => Some(c / weights)
          case None => None
        }
        weightBuffer(xTd, yTd) = RGBA(
          weighted(0).getOrElse(0.0),
          if ( weighted.size>1 ) weighted(1).getOrElse(0.0) else 0.0,
          if ( weighted.size>2 ) weighted(2).getOrElse(0.0) else 0.0
        )
        if (counter == 0) {
          RGBA.BlackTransparent
        } else {
          val mixture = mixedColor / weights
          RGBA(mixture.r, mixture.g, mixture.b, 1.0)
        }
      }
    }
    (blendedTexture.toImage, weightBuffer.toImage)
  }

  def weightedAveraging(
    textures: Seq[PixelImage[RGBA]],
    normalViewRayDeviations: Seq[PixelImage[Double]]
  ): (PixelImage[RGBA], PixelImage[RGBA]) = {
    val textureDomain = textures.head.domain
    val bestUnique: Seq[PixelImage[Double]] = pixelwiseBestVisibleTexture(textureDomain, normalViewRayDeviations)
    val nvrd = normalViewRayDeviations.map(img => img.map(math.max(_, 0.0)))

    val fusedTexture = new PixelImage[RGBA](textureDomain, Repeat[RGBA](), (_: Int, _: Int) => RGBA.BlackTransparent).toBuffer
    val weightImage = new PixelImage[RGBA](textureDomain, Repeat[RGBA](), (_: Int, _: Int) => RGBA.BlackTransparent).toBuffer
    for (xTd <- 0 until textureDomain.width; yTd <- 0 until textureDomain.height) {
      val col = (0 until 3).map { i =>
        val c = textures(i)(xTd, yTd)
        val w = nvrd(i)(xTd, yTd) * c.a
        (c * w, w)
      }.foldLeft((RGBA.BlackTransparent, 0.0))((a, b) => ((a._1 + b._1), (a._2 + b._2)))
      weightImage(xTd, yTd) = RGBA(
        math.max(nvrd(0)(xTd, yTd), 0.0),
        math.max(nvrd(1)(xTd, yTd), 0.0),
        math.max(nvrd(2)(xTd, yTd), 0.0),
        1
      )
      fusedTexture(xTd, yTd) = col._1 / col._2
    }
    (fusedTexture.toImage, weightImage.toImage)
  }

  def blendTexturesByLaplacians(
    textures: Seq[PixelImage[RGBA]],
    normalViewRayDeviations: Seq[PixelImage[Double]]
  ): (PixelImage[RGBA], PixelImage[RGBA]) = {

    val pyramidLevels = 4
    val texturePPPyramids = textures.map(i => GaussPyramid(i, pyramidLevels))
    val nvrdPyramids = normalViewRayDeviations.map(i => GaussPyramid(i, pyramidLevels))

    val levels = texturePPPyramids.head.levels
    val textureDomain = textures.head.domain

    val (fusedOffset, weightOffset) = {
      val nvrd = nvrdPyramids.map(_.level(levels - 1))
      val textureOffsets = texturePPPyramids.map(_.level.last)
      blendTextures(textureOffsets, nvrd)
    }

    var runningLowerLevel = fusedOffset
    val (fusedLaplacians, weightImages) = (for (lvl <- levels - 2 to 0 by -1) yield {
      val nvrd = nvrdPyramids.map(_.level(lvl))
      val upscaledLast = PushPullPyramid.expand(runningLowerLevel)

      implicit val ops = PushPullPyramid.diffOps
      import PixelImage.implicits._
      val laplacians = texturePPPyramids.map(_.level(lvl) - upscaledLast)
      val (lapCombined, wCombined) = combineLaplacianImages(laplacians, nvrd)

      {
        implicit val ops = PushPullPyramid.addOps
        runningLowerLevel = upscaledLast + lapCombined
      }

      (lapCombined, wCombined)
    }).unzip

    val fusedPyramid = new PushPullPyramid(new ImagePyramid[RGBA] {
      val level = fusedLaplacians.reverse :+ fusedOffset
      val levels = level.size
    }, PushPullPyramid.expand)
    val laplacianFusedTexture = fusedPyramid.reconstruct

//    if (true) {
//      val debugDir = "/tmp/texMapDebug/"
//      new File(debugDir).mkdirs()
//
//      for (lvl <- 0 until levels - 1) yield {
//        for (i <- 0 until texturePPPyramids.map(_.level(lvl)).size) {
//          PixelImageIO.write(texturePPPyramids(i).level(lvl).map(_ * 0.5 + RGBA(0.5)), new File(debugDir + s"/debug${lvl}_${i}_laplacians.png"))
//          PixelImageIO.write(nvrdPyramids(i).level(lvl).map(_ * 0.5 + 0.5), new File(debugDir + s"/debug${lvl}_${i}_nvrd.png"))
//        }
//        PixelImageIO.write(weightImages(lvl), new File(debugDir + s"/debug${lvl}_weights.png"))
//        PixelImageIO.write(fusedLaplacians(lvl).map(_ * 0.5 + RGBA(0.5)), new File(debugDir + s"/debug${lvl}_fusedTexture.png"))
//      }
//
//      // handle last level differently
//      val lvl = levels - 1
//      for (i <- 0 until nvrdPyramids.size) {
//        PixelImageIO.write(nvrdPyramids(i).level(lvl), new File(debugDir + s"/debug${lvl}_${i}_nvrd.png"))
//        PixelImageIO.write(texturePPPyramids(i).level(lvl), new File(debugDir + s"/debug${lvl}_${i}_textures.png"))
//      }
//      PixelImageIO.write(weightOffset, new File(debugDir + s"/debug${lvl}_weights.png"))
//      PixelImageIO.write(fusedOffset, new File(debugDir + s"/debug${lvl}_fusedTexture.png"))
//    }

    import PixelImage.implicits._
    val weights = weightImages.foldLeft(weightOffset) { (last: PixelImage[RGBA], next: PixelImage[RGBA]) =>
      next + PushPullPyramid.expand(last)
    }.map(_ / (weightImages.size + 1))

    (laplacianFusedTexture, weights)
  }

  def combineLaplacianImages(
    textures: Seq[PixelImage[RGBA]],
    normalViewRayDeviations: Seq[PixelImage[Double]]
  ): (PixelImage[RGBA], PixelImage[RGBA]) = {

    val textureDomain = textures.head.domain
    assert(textures.forall(tex => textureDomain.width == tex.width && textureDomain.height == tex.height), s"textureDomain:${textureDomain.width}x${textureDomain.height} textures:[" + textures.map(t => s"(${t.width}X${t.height})").foldLeft("")((a: String, b: String) => a + b) + "]")
    assert(normalViewRayDeviations.forall(nvrd => textureDomain.width == nvrd.width && textureDomain.height == nvrd.height), s"textureDomain:${textureDomain.width}x${textureDomain.height} nvrd:[" + normalViewRayDeviations.map(t => s"(${t.width}X${t.height})").foldLeft("")((a: String, b: String) => a + b) + "]")

    val bestUnique: Seq[PixelImage[Double]] = pixelwiseBestVisibleTexture(textureDomain, normalViewRayDeviations)
    val bestSmoothed = bestUnique.map(_.withAccessMode(Repeat[Double]()).filter(IsotropicGaussianFilter(20.0)))

    val possible = textures.map(_.map(_.a))
    val possibleSmoothed = possible.map(p => MaskManipulations.erodeLinearUsing8Neighbourhood(p.withAccessMode(Repeat[Double]()), 20))

    val blendedTextureBuffer = new PixelImage[RGBA](textureDomain, Repeat[RGBA](), (_: Int, _: Int) => RGBA.BlackTransparent).toBuffer
    val weightBuffer = new PixelImage[RGBA](textureDomain, Repeat[RGBA](), (_: Int, _: Int) => RGBA.BlackTransparent).toBuffer

    for (xTd <- (0 until textureDomain.width).par; yTd <- 0 until textureDomain.height) {

      // get colors and reliability weights
      val colorsToBlend = textures.indices.map { (i: Int) =>
        val color = textures(i)(xTd, yTd)
        val deviation = normalViewRayDeviations(i)(xTd, yTd) * 0.5 + 0.5
        val possible = possibleSmoothed(i)(xTd, yTd)
        val best = bestSmoothed(i)(xTd, yTd)

        val valid = (deviation > 0.0) && (possible > 0.0) && (best > 0.0)
        if (valid) {
          val weight = deviation * possible * best
          Some((color, weight))
        } else None
      }

      // blend colors together
      blendedTextureBuffer(xTd, yTd) = if (colorsToBlend.isEmpty) {
        RGBA.BlackTransparent
      } else {
        var mixedColor = RGBA.BlackTransparent
        var weights = 0.0
        var counter = 0

        // weighted color sum and sum of weights
        val w = colorsToBlend.map {
          case Some((color: RGBA, weight: Double)) =>
            mixedColor += color * weight
            weights += weight
            counter += 1
            Some(weight)
          case _ => None
        }

        // normalize weights and save them
        val weighted = w.map {
          case Some(c: Double) => Some(c / weights)
          case None => None
        }
        weightBuffer(xTd, yTd) = RGBA(
          weighted(0).getOrElse(0.0),
          weighted(1).getOrElse(0.0),
          weighted(2).getOrElse(0.0),
          1.0
        )

        // normalize color
        if (counter == 0) {
          RGBA.BlackTransparent
        } else {
          val mixture = mixedColor / weights
          val col = RGBA(mixture.r, mixture.g, mixture.b, 1.0)
          if (col.gray.isNaN) {
            println(col + " " + mixedColor + " " + weights)
            colorsToBlend.map(print)
            println
          }
          col
        }

      }
    }

    (blendedTextureBuffer.toImage, weightBuffer.toImage)
  }

  def pixelwiseBestVisibleTexture(textureDomain: PixelImageDomain, normalViewRayDotProduct: Seq[PixelImage[Double]]): Seq[PixelImage[Double]] = {
    val bestUnique = normalViewRayDotProduct.map(_ => new PixelImage[Double](textureDomain, Repeat[Double](), (_: Int, _: Int) => 0.0).toBuffer)
    for (xTd <- (0 until textureDomain.width).par; yTd <- 0 until textureDomain.height) {
      val v = (0 until normalViewRayDotProduct.size).map { i =>
        (normalViewRayDotProduct(i)(xTd, yTd), i)
      }
      val w = v.sortWith(_._1 > _._1).head._2
      bestUnique(w)(xTd, yTd) = 1.0
    }
    bestUnique.map(_.toImage)
  }

}
