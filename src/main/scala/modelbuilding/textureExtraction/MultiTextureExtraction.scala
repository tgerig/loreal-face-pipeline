/*
 * Copyright 2016 University of Basel, Graphics and Vision Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package modelbuilding.textureExtraction

import java.io.{File, FileWriter}

import faces.mesh.TextureMappedProperty
import scalismo.faces.color.RGBA
import scalismo.faces.image.AccessMode.Repeat
import scalismo.faces.image._
import scalismo.geometry._
import scalismo.mesh.{BarycentricCoordinates, MeshSurfaceProperty, TriangleId, TriangleMesh3D}


trait CCamera {
  def cameraTransform(point: Point[_3D]): Point[_3D]
  def imageToWorld(point: Point[_2D]): Point[_3D]
  def worldToImage(point: Point[_3D]): Point[_2D]
}


object MultiTextureExtraction {

  /**
    * Extracts all individual textures and fuses them based on the passed function blend.
    */
  def extractTextureFromMultipleSources(
                               textureDomain: PixelImageDomain,
                               cameras: Seq[CCamera],
                               images: Seq[PixelImage[RGBA]],
                               mapping: MeshSurfaceProperty[Point[_2D]],
                               mesh: TriangleMesh3D,
                               blend: (Seq[PixelImage[RGBA]], Seq[PixelImage[Double]]) => (PixelImage[RGBA],PixelImage[RGBA]) //= TextureBlending.blendTextures
                             ): (PixelImage[RGBA], PixelImage[RGBA]) = {
    import TextureMappingUtils._

    val bccTexture = calculateBarycentricTextureImage(textureDomain, mapping)

    val (textures, normalViewRayDeviations) = cameras.zip(images).map {
      case (camera: CCamera, image: PixelImage[RGBA]) =>
        extractTextureFromSingleSource(textureDomain, camera, image, mapping, mesh, bccTexture)
    }.unzip

    blend(textures, normalViewRayDeviations)
  }

  /**
    * Extracts the texture and the angular deviation between the surface normal and the inverted view ray.
    */
  def extractTextureFromSingleSource(
                                      textureDomain: PixelImageDomain,
                                      cameras: CCamera,
                                      image: PixelImage[RGBA],
                                      mapping: MeshSurfaceProperty[Point[_2D]],
                                      mesh: TriangleMesh3D,
                                      bccTexture: PixelImage[(Int, Vector[_3D])]
                    ): (PixelImage[RGBA], PixelImage[Double]) = {
    val textureBuffer = PixelImage(textureDomain, (_: Int, _: Int) => RGBA.BlackTransparent).toBuffer
    mapImagePixelsToTexture(textureBuffer, cameras, mesh, image, mapping)
    val normalRayDeviation = fillMissingTexels(textureBuffer, cameras, image, mesh, bccTexture)
    (textureBuffer.toImage, normalRayDeviation)
  }

  /**
    * Maps image pixels to texture texels by:
    * 1. Projecting the pixel location to the 3d mesh surface.
    * 2. Mapping the 3d surface position to the texture.
    * 3. Averaging over all projected pixels that fall onto the same texel.
    */
  def mapImagePixelsToTexture(
                               textureBuffer: ImageBuffer[RGBA],
                               camera: CCamera,
                               mesh: TriangleMesh3D,
                               image: PixelImage[RGBA],
                               mapping: MeshSurfaceProperty[Point[_2D]]
  ): Unit = {

    val w = textureBuffer.width
    val h = textureBuffer.height
    val camCenter = camera.cameraTransform(Point(0, 0, 0))
    val divisors = PixelImage[Int](w, h, (_: Int, _: Int) => 0).toBuffer

    for (xi <- (0 until image.width).par; yi <- 0 until image.height) {
      val sensorPixelPosition = camera.imageToWorld(Point(xi, yi))
      val intersectionPoints = mesh.operations.getIntersectionPointsOnSurface(camCenter, sensorPixelPosition - camCenter)
      if (intersectionPoints.nonEmpty) {
        val pt = intersectionPoints.sortBy { x =>
          val pt: Point[_3D] = mesh.position.onSurface(x._1,x._2)
          (camCenter - pt).norm2
        }.head
        val tIdx = pt._1
        val bcc = pt._2
        if (tIdx.id < mapping.triangulation.triangles.length) {
          val p = mapping.onSurface(tIdx, bcc)
          val coord = TextureMappedProperty.imageInterpolatedCoordinatesFromUV(p, w - 1, h - 1)
          val xt = scala.math.round(coord.x).toInt
          val yt = scala.math.round(coord.y).toInt
          textureBuffer(xt, yt) += image(xi, yi)
          divisors(xt, yt) += 1
        }
      }
    }

    // divide multiple inserted pixel values
    for (xb <- 0 until textureBuffer.width; yb <- 0 until textureBuffer.height) {
      val c = divisors(xb, yb)
      if (c > 1) {
        textureBuffer(xb, yb) /= c
      }
    }
  }

  /**
    * Completes a texture by:
    * 1. Mapping missing texel to the 3d mesh surface.
    * 2. Project the 3d mesh position into the image.
    * 3. Interpolation the color in the image.
    */
  def fillMissingTexels(
                         textureBuffer: ImageBuffer[RGBA],
                         camera: CCamera,
                         image: PixelImage[RGBA],
                         mesh: TriangleMesh3D,
                         bccTexture: PixelImage[(Int, Vector[_3D])]
  ): PixelImage[Double] = {
    val camCenter = camera.cameraTransform(Point(0, 0, 0))

    val normalViewRayDeviation = new PixelImage[Double](textureBuffer.domain, Repeat[Double](), (_: Int, _: Int) => -1.0).toBuffer

    val continuousImage = image.withAccessMode(Repeat()).resample(image.width*2,image.height*2)
    for (xTb <- (0 until textureBuffer.width).par; yTb <- 0 until textureBuffer.height) {
      val texel = textureBuffer(xTb, yTb)
      val bccColor = bccTexture(xTb, yTb)
      val pt3D = getPoint(mesh, TriangleId(bccColor._1), bccColor._2.y, bccColor._2.z)
      if (texel == RGBA.BlackTransparent) {
        val pt2D = camera.worldToImage(pt3D)
        val sensorPixelPosition = camera.imageToWorld(Point(pt2D.x,pt2D.y))
        val intersectionPoints = mesh.operations.getIntersectionPointsOnSurface(camCenter, sensorPixelPosition - camCenter)
        if (intersectionPoints.nonEmpty) {
          val interPoint = intersectionPoints.map { x =>
            mesh.position.onSurface(x._1,x._2)
          }.sortBy{pt =>
            (camCenter - pt).norm2
          }.head

          val mapError = (pt3D - interPoint).norm
          if ( mapError < 1.0e-5) {
            textureBuffer(xTb, yTb) = continuousImage((pt2D.x*2).toInt + 1, (pt2D.y*2).toInt + 1)
          }
        }
      }
      val normal = mesh.vertexNormals.onSurface(TriangleId(bccColor._1), BarycentricCoordinates(bccColor._2.x, bccColor._2.y, bccColor._2.z))
      val nvrd = normal.normalize.dot((camCenter - pt3D).normalize)
      normalViewRayDeviation(xTb, yTb) = if (nvrd.isNaN) -1.0 else nvrd
    }

    normalViewRayDeviation.toImage
  }

  /**
    * Calculates the point given by the mesh, triangle index and the barycentric coordinate.
    *
    * @note could this be replaced when the mesh operations are available in scalismo?
    */
  def getPoint(mesh: TriangleMesh3D, triangleIdx: TriangleId, bc1: Double, bc2: Double): Point[_3D] = {
    val t = mesh.triangulation.triangle(triangleIdx)
    val a = mesh.pointSet.point(t.ptId1)
    val b = mesh.pointSet.point(t.ptId2)
    val c = mesh.pointSet.point(t.ptId3)
    val pt = a + (b - a) * bc1 + (c - a) * bc2
    pt
  }






































  /**
    * Some visualizations around the texture extraction to visualize the process.
    *
    * @note this could be droped once the development of the extraction proceedure is considered finished.
    */
  object debug {
    import TextureMappingUtils._

    def showErrorPixels(
      textureDomain: PixelImageDomain,
      mapping: MeshSurfaceProperty[Point[_2D]],
      tsaiCamera: TsaiCamera,
      image: PixelImage[RGBA],
      mesh: TriangleMesh3D,
      debugDir: String
    ): Unit = {
      val camCenter = tsaiCamera.cameraTransform(Point(0, 0, 0))
      val bccTexture = calculateBarycentricTextureImage(textureDomain, mapping)

      val errorImage = image.toBuffer

      for (xTb <- 0 until textureDomain.width; yTb <- 0 until textureDomain.height) {

        val bccColor = bccTexture(xTb, yTb)
        val pt3D = getPoint(mesh, TriangleId(bccColor._1), bccColor._2.y, bccColor._2.z)
        val pt2D = tsaiCamera.worldToImage(pt3D)
        val sensorPixelPosition = tsaiCamera.imageToWorld(Point(pt2D.x, pt2D.y))
        val intersectionPoints = mesh.operations.getIntersectionPointsOnSurface(camCenter, sensorPixelPosition - camCenter)
        if (intersectionPoints.isEmpty) {
          errorImage(pt2D.x.toInt, pt2D.y.toInt) = RGBA(1.0, 0.0, 0.0, 1.0)
        }
      }
      PixelImageIO.write(errorImage.toImage, new File(s"${debugDir}/numericalNonIntersects.png")).get
    }

    def roundTripErrors(
      textureDomain: PixelImageDomain,
      mapping: MeshSurfaceProperty[Point[_2D]], tsaiCamera: TsaiCamera,
      mesh: TriangleMesh3D,
      debugDir: String
    ) = {

      val writer = new FileWriter(debugDir + "/roundTripErrors.txt")

      {
        var sum = 0.0
        var countSum = 0
        var countNonIntersects = 0
        mesh.pointSet.points.foreach { v =>
          val ic = tsaiCamera.worldToImage(v)
          val sp = tsaiCamera.imageToWorld(Point(ic.x, ic.y))
          val camCenter = tsaiCamera.getOrigin
          val intersections = mesh.operations.getIntersectionPointsOnSurface(camCenter, sp - camCenter)
          if (intersections.nonEmpty) {
            val roundtripError = intersections.map { x =>
              val pt: Point[_3D] = mesh.position.onSurface(x._1, x._2)
              (pt - v).norm
            }.min
            sum += roundtripError
            countSum += 1
          } else {
            countNonIntersects += 1
          }
        }
        writer.write("average round-trip error for vertices: %f".format(sum / countSum))
        writer.write("fraction of missed intersections for vertices: %f".format(countNonIntersects.toDouble / mesh.pointSet.points.size))
      }

      {
        var sum = 0.0
        var countSum = 0
        var countNonIntersects = 0
        val camCenter = tsaiCamera.cameraTransform(Point(0, 0, 0))
        val bccTexture = calculateBarycentricTextureImage(textureDomain, mapping)
        for (xTb <- 0 until textureDomain.width; yTb <- 0 until textureDomain.height) {

          val bccColor = bccTexture(xTb, yTb)
          val pt3D = getPoint(mesh, TriangleId(bccColor._1), bccColor._2.y, bccColor._2.z)
          val pt2D = tsaiCamera.worldToImage(pt3D)
          val sensorPixelPosition = tsaiCamera.imageToWorld(Point(pt2D.x, pt2D.y))
          val intersectionPoints = mesh.operations.getIntersectionPointsOnSurface(camCenter, sensorPixelPosition - camCenter)
          if (intersectionPoints.nonEmpty) {
            val roundtripError = intersectionPoints.map { x =>
              val pt: Point[_3D] = mesh.position.onSurface(x._1,x._2)
              (pt - pt3D).norm
            }.min
            sum += roundtripError
            countSum += 1
          } else {
            countNonIntersects += 1
          }
        }

        writer.write("average round-trip error for texels: %f".format(sum / countSum))
        writer.write("fraction of missed intersections for texels: %f".format(countNonIntersects.toDouble / (textureDomain.width * textureDomain.height)))

      }

      writer.close()
    }

    def mapVerticesToScannerImage(
      image: PixelImage[RGBA],
      tsaiCamera: TsaiCamera,
      mesh: TriangleMesh3D,
      debugDir: String
    ) = {
      val imageBuffer = image.toBuffer
      mesh.pointSet.points.foreach { pt =>
        val pid = tsaiCamera.worldToImage(pt)
        val ptm = tsaiCamera.windowTransform(image.width, image.height)(tsaiCamera.pointShader(image.width, image.height)(pt))
        imageBuffer(pid.x.toInt, pid.y.toInt) = RGBA(1.0, 0.0, 0.0, 1.0)
        imageBuffer(ptm.x.toInt, ptm.y.toInt) = RGBA(0.0, 1.0, 0.0, 1.0)
      }
      PixelImageIO.write(imageBuffer.toImage, new File(s"${debugDir}/verticesInScanner.png")).get
    }

    def mapTriangleCentersToTextureMap(
      textureBuffer: PixelImageDomain,
      mapping: MeshSurfaceProperty[Point[_2D]],
      debugDir: String
    ) = {
      val h = textureBuffer.height
      val w = textureBuffer.width
      val centerBuffer = ImageBuffer(w, h, Array.fill[RGBA](w * h)(RGBA(0.0, 0.0, 0.0, 0.0)))
      val bcc = BarycentricCoordinates(0.33333, 0.33333, 0.33333)
      mapping.triangulation.triangleIds.foreach { tid =>
        val pt = mapping.onSurface(tid, bcc)
        val coord = TextureMappedProperty.imageInterpolatedCoordinatesFromUV(pt, w - 1, h - 1)
        val x = scala.math.round(coord.x).toInt
        val y = scala.math.round(coord.y).toInt

        centerBuffer(x, y) = RGBA(1.0, 0.0, 0.0, 1.0)
      }
      PixelImageIO.write(centerBuffer.toImage, new File(s"${debugDir}/triangleInTextureMap.png")).get
    }

    def tsaiCameraDistortion(
      cam: TsaiCamera,
      debugDir: String
    ): Unit = {
      val w = 3504
      val h = 2336
      val buffer = PixelImage[RGBA](w, h, (_: Int, _: Int) => RGBA.BlackTransparent).toBuffer
      val stream = for (x <- 0 until w; y <- 0 until h) yield {
        val z = cam.imageToWorld(Point(x, y))
        val u = cam.distortedPlaneToImage(cam.planeToDistortedPlane(cam.cameraToPlane(cam.worldToCamera(z))))
        val v = cam.distortedPlaneToImage(cam.cameraToPlane(cam.worldToCamera(z)))
        buffer(x, y) = (RGBA(u.x - v.x, 0, u.y - v.y, 10) / 20 + RGBA(0.5)).clamped
        (u.x - v.x, u.y - v.y)
      }

      val writer = new FileWriter(debugDir + "/distortionError.txt")
      writer.write("max deviation on x-axis: " + stream.map(_._1).max)
      writer.write("max deviation on y-axis: " + stream.map(_._2).max)
      PixelImageIO.write(buffer.toImage, new File(s"${debugDir}/distortion.png")).get
    }
  }

}
