package ch.unibas.cs.gravis.facepipeline

import java.io.File

import faces.landmarks.{TLMSLandmark3D, TLMSLandmarksIO}
import faces.mesh.{ColorNormalMesh3D, GravisMeshIO}
import faces.mesh.io.PLYMesh
import faces.momo.{MoMo, ModelIO}
import registration.FaceMask
import scalismo.geometry.{Landmark, Point, _3D}
import scalismo.io.{LandmarkIO, MeshIO, StatismoIO}
import scalismo.mesh.TriangleMesh
import scalismo.statisticalmodel.StatisticalMeshModel
import scalismotools.common.repo.Repository

import scala.io.Source
import scala.reflect.io.Path
import scala.util.{Failure, Success, Try}

/**
 * Created by luetma00 on 20.01.17.
 */
object LOrealDataProvider extends DataProvider {

  case object Neutral extends ExpressionType { override def toString: String = "" }
  case object Sadness extends ExpressionType { override def toString: String = "_sadness" }
  case object Surprise extends ExpressionType { override def toString: String = "_surprise" }
  case object Disgust extends ExpressionType { override def toString: String = "_disgust" }
  case object Fear extends ExpressionType { override def toString: String = "_fear" }
  case object Joy extends ExpressionType { override def toString: String = "_joy" }
  case object Anger extends ExpressionType { override def toString: String = "_anger" }

  object Expressions {
    def expressionList(): Seq[ExpressionType] = Seq(Neutral)
  }


  object BlackList {
    def blackList(): Seq[LorealID] =  Seq(
      LorealID("2011_07_25_187_164",""), // Ganzes Gesicht
      LorealID("2011_07_11_033_020",""), // Rechtes Ohr
      LorealID("2011_08_04_202_044",""), // Beide Ohren
      LorealID("2011_07_25_157_086",""), // Linkes Ohr
      LorealID("2011_08_04_212_080",""), // Beide Ohren
      LorealID("2011_07_25_138_002",""), // Ein Ohr etwas zu klein
      LorealID("2011_07_19_004_014","")  // Ohren
    )
  }

  case object RAW extends MaskType {override def toString: String = ""}
  case object BFM extends MaskType { override def toString: String = "_BFM" }

  object Masks {
    def maskList(): Seq[MaskType] = Seq()
  }

  case class LorealID(override val id: String, override val raceTag: String) extends Person

  object LorealID {
    def fromFilename(filename: String): LorealID = {
      LorealID(filename.substring(0, 18), "")
    }
  }

  case object Line extends DataFlag { override def toString: String = "_lines"}
  case object Basel extends DataFlag { override def toString: String = ""}
  case object Merged extends DataFlag { override def toString: String = "_merged"}
  case object Original extends DataFlag { override def toString: String = "" }
  case object Aligned extends DataFlag { override def toString: String = "_aligned" }

  object Flags {
    def lmFlagList(): Seq[DataFlag] = Seq(Line, Original, Aligned)
  }

  private def setFileAccessMode(filename: String): Unit = setFileAccessMode(new File(filename))
  private def setFileAccessMode(path: Path): Unit = setFileAccessMode(path.jfile)
  private def setFileAccessMode(file: File): Unit = {
    file.setReadable(true,false)
    file.setWritable(true,false)
  }

  def isLineLm(l : Landmark[_3D]) : Boolean = {
    l.id.split('.').last.exists(c => c.isDigit)
  }

  val repositoryRoot = Path("/export/faces/projects/pami-ppm2017/loreal-face-pipeline/")

  override def incoming: LOrealDataProvider.Incoming = {

    new LOrealDataProvider.Incoming {

      val incomingPath = repositoryRoot / "data" / "incoming"

      override def reference: LOrealDataProvider.Reference = new LOrealDataProvider.Reference {

        val referencePath = incomingPath / "reference"

        override def loadFaceMask(): Try[FaceMask] = {

          for {
            mask <- MeshIO.readScalarMeshField[Float](new File(referencePath.jfile, "facemask-l7-nonose.vtk")).map(_.map(_.toInt))
            semantic_mask <- MeshIO.readScalarMeshField[Int](new File(referencePath.jfile, "mask-l7-semantic.vtk"))
          } yield {
            FaceMask(mask,semantic_mask)
          }

        }

        override def loadMesh(expression: ExpressionType): Try[TriangleMesh[_3D]] = {
          val mshPath = referencePath / "mesh"
          expression match {
            case Neutral => GravisMeshIO.readMSHMesh(new File(mshPath.jfile, "mean2012_l7_bfm_nomouth.msh.gz"))
              .map(_.triangleMesh)
            case Sadness => GravisMeshIO.readMSHMesh(new File(mshPath.jfile, "mean2015.1_l7_bfm_nomouth-sadness.msh.gz"))
              .map(_.triangleMesh)
            case Surprise => GravisMeshIO.readMSHMesh(new File(mshPath.jfile, "mean2015.1_l7_bfm_nomouth-surprise.msh.gz"))
              .map(_.triangleMesh)
            case Disgust => GravisMeshIO.readMSHMesh(new File(mshPath.jfile, "mean2015.1_l7_bfm_nomouth-disgust.msh.gz"))
              .map(_.triangleMesh)
            case Fear => GravisMeshIO.readMSHMesh(new File(mshPath.jfile, "mean2015.1_l7_bfm_nomouth-fear.msh.gz"))
              .map(_.triangleMesh)
            case Joy => GravisMeshIO.readMSHMesh(new File(mshPath.jfile, "mean2015.1_l7_bfm_nomouth-joy.msh.gz"))
              .map(_.triangleMesh)
            case Anger => GravisMeshIO.readMSHMesh(new File(mshPath.jfile, "mean2015.1_l7_bfm_nomouth-anger.msh.gz"))
              .map(_.triangleMesh)
          }
        }

        override def loadLandmarks(expression: ExpressionType): Try[Seq[Landmark[_3D]]] = {
          val lmPath = referencePath / "landmarks"
            LandmarkIO.readLandmarksJson[_3D](new File(lmPath.jfile, s"reference-${expression.toString}-with-eyes.json"))
        }

        override def saveLineLandmarks(expression: ExpressionType, landmarks: Seq[Landmark[_3D]]): Try[Unit] = {
          val lmPath = referencePath / "landmarks"
          val res = LandmarkIO.writeLandmarksJson[_3D](landmarks.toIndexedSeq, new File(lmPath.jfile, s"reference-${expression.toString}-all.json"))
          res match {
            case Success(_) => setFileAccessMode(lmPath)
            case _ =>
          }
          res
        }

        override def saveLandmarks(expression: ExpressionType, landmarks: Seq[Landmark[_3D]]): Try[Unit] = {
          val lmPath = referencePath / "landmarks"
          val res = LandmarkIO.writeLandmarksJson[_3D](landmarks.toIndexedSeq, new File(lmPath.jfile, s"reference-${expression.toString}-with-eyes.json"))
          res match {
              case Success(_) => setFileAccessMode(lmPath)
              case _ =>
            }
          res
        }

        override def loadLineLandmarks(expression: ExpressionType): Try[Seq[Landmark[_3D]]] = {
          val lmPath = referencePath / "landmarks"
          LandmarkIO.readLandmarksJson[_3D](new File(lmPath.jfile, s"reference-${expression.toString}-all.json")).map(_.filter(isLineLm))//.map(_.filterNot(_.id.contains("ear")))
        }

      }

      def landmarksPath(id: Person, expression: ExpressionType, mask: MaskType = RAW, flag: DataFlag = Basel): Path = {
        incomingPath / "landmarks" / s"${id.id}${expression}${id.raceTag}${mask}$flag.tlms"
      }

      override def loadLandmarks(id: Person, expression: ExpressionType): Try[Seq[Landmark[_3D]]] = loadLandmarks(id, expression, RAW, Basel)
      override def loadLandmarks(id: Person, expression: ExpressionType, mask: MaskType): Try[Seq[Landmark[_3D]]] = loadLandmarks(id, expression, mask, Basel)
//      override def loadLandmarks(id: Person, expression: ExpressionType, mask: MaskType, flag: DataFlag): Try[Seq[Landmark[_3D]]] = loadLandmarks(id,expression,mask,flag)
      override def loadLineLandmarks(id: Person, expression: ExpressionType): Try[Seq[Landmark[_3D]]] = loadLineLandmarks(id, expression, RAW, Line)
      override def loadLineLandmarks(id: Person, expression: ExpressionType, mask: MaskType): Try[Seq[Landmark[_3D]]] = loadLineLandmarks(id, expression, mask, Line)
      override def loadLineLandmarks(id: Person, expression: ExpressionType, mask: MaskType, flag: DataFlag): Try[Seq[Landmark[_3D]]] = loadAllLandmarks(id,expression,mask,flag)//.map(_.filterNot(_.id.contains("ear")))

      override def loadLandmarks(id: Person, expression: ExpressionType, mask: MaskType, flag : DataFlag) : Try[Seq[Landmark[_3D]]] = {

        val path = landmarksPath(id, expression, mask, flag)
        println(path)
        readTLMS(path.jfile) match {
          case Success(tlmsLandmarks) => Success(tlmsLandmarks.filter(_.visible).map(_.toLandmark))
          case Failure(t) => Failure(t)
        }

      }

      private def loadAllLandmarks(id: Person, expression: ExpressionType, mask: MaskType, flag : DataFlag) : Try[Seq[Landmark[_3D]]] = {

        val path = landmarksPath(id, expression, mask, Line)
        println(path)
        readTLMS(path.jfile) match {
          case Success(tlmsLandmarks) => Success(tlmsLandmarks.filter(_.visible).map(_.toLandmark).filter(isLineLm))
          case Failure(t) => Failure(t)
        }

      }
      /** read TLMS 3D format from a stream (format: "id visible x y z") */
      def readTLMS(file: File): Try[IndexedSeq[TLMSLandmark3D]] = Try {
        val lines = Source.fromFile(file).getLines()
        lines.map { line =>
          val fields = line.split("\\s+").map(_.trim)
          val name = fields(0)
          val visibility: Boolean = fields(1).toInt > 0
          val x = fields(2).toFloat
          val y = fields(3).toFloat
          val z = fields(4).toFloat
          TLMSLandmark3D(name, Point(x, y, z), visibility)
        }.toIndexedSeq
      }

      override def saveLandmarks(id: Person, expression: ExpressionType, landmarks: Seq[Landmark[_3D]]): Try[Unit] = saveLandmarks(id, expression, RAW, Basel, landmarks)
      override def saveLandmarks(id: Person, expression: ExpressionType, mask: MaskType, landmarks: Seq[Landmark[_3D]]): Try[Unit] = saveLandmarks(id, expression, mask, Basel, landmarks)
      override def saveLandmarks(id: Person, expression: ExpressionType, mask: MaskType, flag: DataFlag, landmarks: Seq[Landmark[_3D]]): Try[Unit] = {
        val path = landmarksPath(id, expression, mask, flag)
        val tlms = landmarks.map { lm =>
          TLMSLandmark3D(lm.id, lm.point, visible = true)
        }.toIndexedSeq
        val res = TLMSLandmarksIO.write3D(tlms, path.jfile)
        res match {
          case Success(_) => setFileAccessMode(path)
          case _ =>
        }
        res
      }

      def meshPath(id: Person, expression: ExpressionType, mask: MaskType = RAW, flag: DataFlag = Original, shellNumber: Option[Int] = None, dataType: String = "msh.gz"): Path = {
        incomingPath / "mesh" / s"${id.id}${expression}${shellNumber.map(f => s"_$f").getOrElse("")}${mask}$flag.$dataType"
      }

      def loadShellMeshes(id: Person, expression: ExpressionType) : Seq[Try[TriangleMesh[_3D]]]= {

        for(s <- Seq(1,2)) yield {

          val path = meshPath(id,expression,RAW,Original,Some(s))
          GravisMeshIO.readMSHMesh(path.jfile).map(f => f.triangleMesh)

        }

      }

      override def loadMesh(id: Person, expression: ExpressionType) = loadMesh(id, expression, RAW, Original)
      override def loadMesh(id: Person, expression: ExpressionType, mask: MaskType) = loadMesh(id, expression, mask, Original)
      override def loadMesh(id: Person, expression: ExpressionType, mask: MaskType, flag: DataFlag): Try[TriangleMesh[_3D]] = {
        val path = meshPath(id, expression, mask, flag, dataType = "ply")
        PLYMesh.readTriangleMesh3D(path.toString)
      }

      override def loadColoredMesh(id: Person, expression: ExpressionType): Try[ColorNormalMesh3D] = loadColoredMesh(id, expression, RAW, Original)
      override def loadColoredMesh(id: Person, expression: ExpressionType, mask: MaskType): Try[ColorNormalMesh3D] = loadColoredMesh(id, expression, mask, Original)
      override def loadColoredMesh(id: Person, expression: ExpressionType, mask: MaskType, flag: DataFlag): Try[ColorNormalMesh3D] = {
        val path = meshPath(id, expression, mask, flag, dataType = "ply")
        PLYMesh.readColorNormalMesh3D(path.toString)
      }

      override def saveMesh(id: Person, expression: ExpressionType, mesh: TriangleMesh[_3D]): Try[Unit] = saveMesh(id, expression, RAW, Original, mesh)
      override def saveMesh(id: Person, expression: ExpressionType, mask: MaskType, mesh: TriangleMesh[_3D]): Try[Unit] = saveMesh(id, expression, mask, Original, mesh)
      override def saveMesh(id: Person, expression: ExpressionType, mask: MaskType, flag: DataFlag, mesh: TriangleMesh[_3D]): Try[Unit] = {
        val path = meshPath(id, expression, mask, flag,dataType = "ply")
        PLYMesh.writePLY(mesh,path.toString)
        Success(Unit)
      }

      override def saveMesh(id: Person, expression: ExpressionType, mesh: ColorNormalMesh3D): Try[Unit] = saveMesh(id, expression, RAW, Original, mesh)
      override def saveMesh(id: Person, expression: ExpressionType, mask: MaskType, mesh: ColorNormalMesh3D): Try[Unit] = saveMesh(id, expression, mask, Original, mesh)
      override def saveMesh(id: Person, expression: ExpressionType, mask: MaskType, flag: DataFlag, mesh: ColorNormalMesh3D): Try[Unit] = {
        val path = meshPath(id, expression, mask, flag, dataType = "ply")
        PLYMesh.writePLY(mesh,path.toString)
        Success(Unit)
      }

      override def ids(expression: ExpressionType): Seq[Person] = {
        new File(incomingPath.jfile, "mesh").listFiles()
          .filter(_.getName.endsWith(".msh.gz"))
//          .filter(_.getName.contains(RAW.toString))
          .filter(_.getName.contains(expression.toString))
          .map(file => LorealID.fromFilename(file.getName))
          .toSeq
      }

      override def saveLineLandmarks(id: _root_.ch.unibas.cs.gravis.facepipeline.LOrealDataProvider.Person, expression: ExpressionType, landmarks: Seq[Landmark[_3D]]): Try[Unit] = ???

      override def exists(id: _root_.ch.unibas.cs.gravis.facepipeline.LOrealDataProvider.Person, expression: ExpressionType, mask: MaskType, flag: DataFlag): Boolean = {

        val path = meshPath(id, expression, mask, flag, dataType = "ply").jfile
        path.exists()

      }
    }
  }

  override def registration: LOrealDataProvider.SurfaceRegistration = {

    new LOrealDataProvider.SurfaceRegistration {
      val registrationPath = repositoryRoot / "data" / "registered"

      val referencePath = registrationPath / "reference"

      val modelPath = referencePath / "gpmodels"

      override def loadPriorModel(expression: ExpressionType): Try[StatisticalMeshModel] = {
        expression match {
          case Neutral => StatismoIO.readStatismoMeshModel(new File(modelPath.jfile, "face-model-neutral.h5"))
          case Sadness => StatismoIO.readStatismoMeshModel(new File(modelPath.jfile, "face-model-sadness.h5"))
          case Surprise => StatismoIO.readStatismoMeshModel(new File(modelPath.jfile, "face-model-surprise.h5"))
          case Disgust => StatismoIO.readStatismoMeshModel(new File(modelPath.jfile, "face-model-disgust.h5"))
          case Fear => StatismoIO.readStatismoMeshModel(new File(modelPath.jfile, "face-model-fear.h5"))
          case Joy => StatismoIO.readStatismoMeshModel(new File(modelPath.jfile, "face-model-joy.h5"))
          case Anger => StatismoIO.readStatismoMeshModel(new File(modelPath.jfile, "face-model-anger.h5"))
        }
      }

      override def savePriorModel(model: StatisticalMeshModel, expression: ExpressionType): Try[Unit] = {
        expression match {
          case Neutral => StatismoIO.writeStatismoMeshModel(model, new File(modelPath.jfile, "face-model-neutral.h5"))
          case Sadness => StatismoIO.writeStatismoMeshModel(model, new File(modelPath.jfile, "face-model-sadness.h5"))
          case Surprise => StatismoIO.writeStatismoMeshModel(model, new File(modelPath.jfile, "face-model-surprise.h5"))
          case Disgust => StatismoIO.writeStatismoMeshModel(model, new File(modelPath.jfile, "face-model-disgust.h5"))
          case Fear => StatismoIO.writeStatismoMeshModel(model, new File(modelPath.jfile, "face-model-fear.h5"))
          case Joy => StatismoIO.writeStatismoMeshModel(model, new File(modelPath.jfile, "face-model-joy.h5"))
          case Anger => StatismoIO.writeStatismoMeshModel(model, new File(modelPath.jfile, "face-model-anger.h5"))
        }

      }

      def meshPath(id: Person, expression: ExpressionType, mask: MaskType = RAW, flag: DataFlag = Original): Path = {
        registrationPath / "mesh" / s"${id.id}${expression}${id.raceTag}${mask}${flag}.ply"
      }

      override def loadMesh(id: Person, expression: ExpressionType): Try[TriangleMesh[_3D]] = {
        val path = meshPath(id, expression)
        PLYMesh.readTriangleMesh3D(path.toString())
      }

      override def loadMesh(id: Person, expression: ExpressionType, mask: MaskType): Try[TriangleMesh[_3D]] = ???
      override def loadMesh(id: Person, expression: ExpressionType, mask: MaskType, flag: DataFlag): Try[TriangleMesh[_3D]] = ???
      override def loadColoredMesh(id: Person, expression: ExpressionType): Try[ColorNormalMesh3D] = ???
      override def loadColoredMesh(id: Person, expression: ExpressionType, mask: MaskType): Try[ColorNormalMesh3D] = ???
      override def loadColoredMesh(id: Person, expression: ExpressionType, mask: MaskType, flag: DataFlag): Try[ColorNormalMesh3D] = ???

      override def saveMesh(id: Person, expression: ExpressionType, mesh: TriangleMesh[_3D]): Try[Unit] = {
        val path = meshPath(id, expression)
        PLYMesh.writePLY(mesh, path.toString())
        setFileAccessMode(path)
        Success(Unit)
      }

      override def saveMesh(id: Person, expression: ExpressionType, mask: MaskType, mesh: TriangleMesh[_3D]): Try[Unit] = ???
      override def saveMesh(id: Person, expression: ExpressionType, mask: MaskType, flag: DataFlag, mesh: TriangleMesh[_3D]): Try[Unit] = ???

      override def saveMesh(id: Person, expression: ExpressionType, mesh: ColorNormalMesh3D): Try[Unit] = ???
      override def saveMesh(id: Person, expression: ExpressionType, mask: MaskType, mesh: ColorNormalMesh3D): Try[Unit] = ???
      override def saveMesh(id: Person, expression: ExpressionType, mask: MaskType, flag: DataFlag, mesh: ColorNormalMesh3D): Try[Unit] = ???

      override def ids(expression: ExpressionType): Seq[Person] = {
        new File(registrationPath.jfile, "mesh").listFiles()
          .filter(_.getName.endsWith("ply"))
          .filter(_.getName.contains(expression.toString))
          .map(file => LorealID.fromFilename(file.getName))
          .toSeq
      }

      override def exists(id: _root_.ch.unibas.cs.gravis.facepipeline.LOrealDataProvider.Person, expression: ExpressionType, mask: MaskType, flag: DataFlag): Boolean = ???
    }
  }

  override def model : LOrealDataProvider.ModelBuilding = LOrealModelBuilding

  object LOrealModelBuilding extends LOrealDataProvider.ModelBuilding{
    val modelBuildingPath = repositoryRoot / "data" / "modelbuilding"

    val modelDirectoryPath = modelBuildingPath / "model"

    val colorExtractdMeshPath = modelBuildingPath / "mesh"

    def meshPath(id: Person, expression: ExpressionType, mask: MaskType = RAW, flag: DataFlag = Original): Path = {
      colorExtractdMeshPath / s"${id.id}${expression}${id.raceTag}${mask}$flag.ply"
    }

    def loadColoredMesh(id: Person, expression: ExpressionType, mask: MaskType = RAW, flag: DataFlag = Original): Try[ColorNormalMesh3D] = {
      val path = meshPath(id, expression, mask, flag)
      PLYMesh.readColorNormalMesh3D(path.toString())
    }

    def saveColoredMesh(id: Person, expression: ExpressionType, mask: MaskType = RAW, flag: DataFlag = Original, mesh: ColorNormalMesh3D): Try[Unit] = {
      val path = meshPath(id, expression, mask, flag)
      PLYMesh.writePLY(mesh, path.toString())
      setFileAccessMode(path)
      Success(Unit)
    }

    def modelPath(mask: MaskType): Path = {
      modelDirectoryPath / s"bu3d_pami17${mask}.h5" // @todo think about registration identifier in name
    }

    override def saveModel(mask: MaskType, momo: MoMo): Try[Unit] = {
      val path = modelPath(mask)
      val res = ModelIO.saveMoMo(momo, path.jfile, "")
      res match {
        case Success(_) => setFileAccessMode(path)
        case _ =>
      }
      res
    }

    override def loadModel(mask: MaskType): Try[MoMo] = {
      ModelIO.loadMoMo(modelPath(mask).jfile, "")
    }

    override def loadShapeModel(mask: MaskType): Try[StatisticalMeshModel] = ???
  }

  override def fitting: LOrealDataProvider.Fitting = {
    new LOrealDataProvider.Fitting {

    }
  }

}
