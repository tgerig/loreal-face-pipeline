package ch.unibas.cs.gravis.facepipeline

import faces.mesh.ColorNormalMesh3D
import faces.momo.MoMo
import registration.FaceMask
import scalismo.geometry.{Landmark, _3D}
import scalismo.mesh.{ScalarMeshField, TriangleMesh}
import scalismo.statisticalmodel.{LowRankGaussianProcess, StatisticalMeshModel}

import scala.util.Try

trait ExpressionType {
  override def toString : String
}

trait MaskType {
  override def toString: String
}

trait DataFlag {
  override def toString: String
}

trait DataProvider {

  trait Person {
    def id: String
    def raceTag: String
  }

  trait WithMesh {
    def loadMesh(id: Person, expression: ExpressionType): Try[TriangleMesh[_3D]]
    def loadMesh(id: Person, expression: ExpressionType, mask: MaskType): Try[TriangleMesh[_3D]]
    def loadMesh(id: Person, expression: ExpressionType, mask: MaskType, flag: DataFlag): Try[TriangleMesh[_3D]]
    def exists(id: Person, expression: ExpressionType, mask: MaskType, flag: DataFlag): Boolean
    def loadColoredMesh(id: Person, expression: ExpressionType): Try[ColorNormalMesh3D]
    def loadColoredMesh(id: Person, expression: ExpressionType, mask: MaskType): Try[ColorNormalMesh3D]
    def loadColoredMesh(id: Person, expression: ExpressionType, mask: MaskType, flag: DataFlag): Try[ColorNormalMesh3D]
    def saveMesh(id: Person, expression: ExpressionType, mesh: TriangleMesh[_3D]): Try[Unit]
    def saveMesh(id: Person, expression: ExpressionType, mask: MaskType, mesh: TriangleMesh[_3D]): Try[Unit]
    def saveMesh(id: Person, expression: ExpressionType, mask: MaskType, flag: DataFlag, mesh: TriangleMesh[_3D]): Try[Unit]
    def saveMesh(id: Person, expression: ExpressionType, mesh: ColorNormalMesh3D): Try[Unit]
    def saveMesh(id: Person, expression: ExpressionType, mask: MaskType, mesh: ColorNormalMesh3D): Try[Unit]
    def saveMesh(id: Person, expression: ExpressionType, mask: MaskType, flag: DataFlag, mesh: ColorNormalMesh3D): Try[Unit]
  }

  trait WithLandmarks {
    def loadLandmarks(id: Person, expression: ExpressionType): Try[Seq[Landmark[_3D]]]
    def loadLandmarks(id: Person, expression: ExpressionType, mask: MaskType): Try[Seq[Landmark[_3D]]]
    def loadLandmarks(id: Person, expression: ExpressionType, mask: MaskType, flag: DataFlag): Try[Seq[Landmark[_3D]]]
    def saveLandmarks(id: Person, expression: ExpressionType, landmarks: Seq[Landmark[_3D]]): Try[Unit]
    def saveLandmarks(id: Person, expression: ExpressionType, mask: MaskType, landmarks: Seq[Landmark[_3D]]): Try[Unit]
    def saveLandmarks(id: Person, expression: ExpressionType, mask: MaskType, flag: DataFlag, landmarks: Seq[Landmark[_3D]]): Try[Unit]
  }

  trait WithShells {
    def loadShellMeshes(id: Person, expression: ExpressionType) : Seq[Try[TriangleMesh[_3D]]]
  }

  trait WithLineLandmarks {

    def loadLineLandmarks(id: Person, expression: ExpressionType, mask: MaskType): Try[Seq[Landmark[_3D]]]
    def loadLineLandmarks(id: Person, expression: ExpressionType, mask: MaskType, flag: DataFlag): Try[Seq[Landmark[_3D]]]
    def saveLineLandmarks(id: Person, expression: ExpressionType, landmarks: Seq[Landmark[_3D]]): Try[Unit]
//    def saveLineLandmarks(id: Person, expression: ExpressionType, mask: MaskType, landmarks: Seq[Landmark[_3D]]): Try[Unit]
//    def saveLineLandmarks(id: Person, expression: ExpressionType, mask: MaskType, flag: DataFlag, landmarks: Seq[Landmark[_3D]]): Try[Unit]
    def loadLineLandmarks(id: Person, expression: ExpressionType): Try[Seq[Landmark[_3D]]]

  }

  trait WithIds {
    def ids(expression: ExpressionType): Seq[Person]
  }

  trait Reference {
    def loadMesh(expression: ExpressionType): Try[TriangleMesh[_3D]]
    def loadFaceMask(): Try[FaceMask]
    def loadLandmarks(expression: ExpressionType): Try[Seq[Landmark[_3D]]]
    def saveLandmarks(expression: ExpressionType, landmarks : Seq[Landmark[_3D]]): Try[Unit]
    def saveLineLandmarks(expression: ExpressionType, landmarks : Seq[Landmark[_3D]]): Try[Unit]
    def loadLineLandmarks(expression: ExpressionType): Try[Seq[Landmark[_3D]]]
  }

  trait Incoming extends WithIds with WithMesh with WithLandmarks with WithLineLandmarks with WithShells {

    def reference: Reference
  }

  trait SurfaceRegistration extends WithIds with WithMesh {
    def loadPriorModel(expression: ExpressionType): Try[StatisticalMeshModel]
    def savePriorModel(model: StatisticalMeshModel, expressionType: ExpressionType): Try[Unit]
  }

  trait ModelBuilding {
    def loadModel( mask: MaskType ) : Try[MoMo]
    def loadShapeModel( mask: MaskType ) : Try[StatisticalMeshModel]
    def saveModel( mask: MaskType, momo: MoMo ) : Try[Unit]
  }

  trait Fitting {}

  def incoming: Incoming

  def registration: SurfaceRegistration

  def model: ModelBuilding

  def fitting: Fitting
}

