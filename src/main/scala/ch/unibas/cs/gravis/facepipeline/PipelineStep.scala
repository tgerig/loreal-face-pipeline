package ch.unibas.cs.gravis.facepipeline

/**
  * Created by luetma00 on 20.01.17.
  */
trait PipelineStep {

  def run() : Unit
}
