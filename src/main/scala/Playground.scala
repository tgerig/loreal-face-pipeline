/*
 * Copyright University of Basel, Graphics and Vision Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.{File, PrintWriter}

import ch.unibas.cs.gravis.facepipeline.LOrealDataProvider.Person
import ch.unibas.cs.gravis.facepipeline.{ExpressionType, LOrealDataProvider}
import faces.gui.GUIBlock._
import faces.gui.ImagePanel
import faces.mesh.GravisMSH.MSHMesh
import faces.mesh.{ColorNormalMesh3D, GravisMeshIO, VertexColorMesh3D}
import faces.mesh.io.PLYMesh
import faces.momo.ModelIO
import faces.parameters.{ImageSize, ParametricRenderer, RenderParameter}
import faces.render._
import modelbuilding.textureExtraction.CCamera
import scalismo.common.UnstructuredPointsDomain
import scalismo.faces.color.RGBA
import scalismo.faces.image.{PixelImage, PixelImageIO}
import scalismo.geometry.{Point, Vector, _2D, _3D}
import scalismo.io.MeshIO
import scalismo.mesh.{BarycentricCoordinates, _}
import scalismo.registration.LandmarkRegistration
import tools.Frog

import scala.reflect.io.Path
import scala.util.{Failure, Success}

object Playground extends App {




















  def analyzeLOrealTextureExtraction: Unit = {
    val id = "2011_07_25_165_116"
    val dir = Path("/export/faces/projects/pami-ppm2017/loreal-face-pipeline/data")
    val exp = Path(s"/export/faces/data_loreal/registration/dataset_001/marked")

    val regFile = dir / ("registered/mesh/" + id + ".ply")
    val frogFiles = (1 to 2) map (i => dir / (s"incoming/mesh/${id}_${i}.frog"))
    val imageFiles = (1 to 2) map (i => exp / (s"${id}/cleaned/${id}_${i}.png"))

    val mesh = PLYMesh.readTriangleMesh3D(regFile.toString).get
    val frogs = frogFiles map (f => Frog.readFromFile(f.toString).get)
    val imgs = imageFiles map (f => PixelImageIO.read[RGBA](f.jfile).get)

    val cams = for (frog <- frogs) yield {
      new CCamera {

        def objectTransform = frog.pose.transform

        def objectTransformInv = frog.pose.transform.inverted

        def modelView = frog.view.viewTransform

        def modelViewInv = frog.view.viewTransform.inverted

        def projection = frog.camera.projection

        def windowTransform = frog.imageSize.screenTransform

        def windowTransformInv = frog.imageSize.screenTransform.inverted

        override def worldToImage(point: Point[_3D]): Point[_2D] = {
          val pt = windowTransform(projection(modelView(objectTransform(point))))
          Point(pt.x, pt.y)
        }

        override def cameraTransform(point: Point[_3D]): Point[_3D] = objectTransformInv(modelView(point))

        override def imageToWorld(point: Point[_2D]): Point[_3D] = objectTransformInv(modelViewInv(projection.inverse(windowTransformInv(Point(point.x, point.y, frog.value("f").get)))))
      }
    }

    //  points(frogs.head, imgs.head, mesh)
    //  points(cams.head, imgs.head, mesh)
    checkRoundtrips(cams.head, imgs.head, mesh)
  }

  def points(frog: Frog, img: PixelImage[RGBA], mesh: TriangleMesh3D): Unit = {
    val imb = img.map(_.toRGB.toRGBA).toBuffer
    val cam = frog.camera()
    val pose = frog.pose()
    val wt = frog.imageSize().screenTransform
    mesh.pointSet.points.foreach { p3d =>
      val p2d = wt(cam.projection(pose.transform(p3d)))
      for (
        dx <- -2 to 2;
        dy <- -2 to 2
      ) {
        imb(p2d.x.toInt + dx, p2d.y.toInt + dy) = RGBA(1, 0, 0, 1)
      }
    }
    val im = imb.toImage.interpolate
    val fact = 1024.0 / imb.height
    val scaled = PixelImage((imb.width * fact).toInt, 1024, (x, y) => im((x / fact).toInt, (y / fact).toInt))
    ImagePanel(scaled).displayIn("frog - points")
  }

  def points(cam: CCamera, img: PixelImage[RGBA], mesh: TriangleMesh3D): Unit = {
    val imb = img.map(_.toRGB.toRGBA).toBuffer
    mesh.pointSet.points.foreach { p3d =>
      val p2d = cam.worldToImage(p3d)
      for (
        dx <- -2 to 2;
        dy <- -2 to 2
      ) {
        try {
          imb(p2d.x.toInt + dx, p2d.y.toInt + dy) = RGBA(1, 0, 0, 1)
        } catch {
          case e: Throwable =>
        }
      }
    }
    val im = imb.toImage.interpolate
    val fact = 1024.0 / imb.height
    val scaled = PixelImage((imb.width * fact).toInt, 1024, (x, y) => im((x / fact).toInt, (y / fact).toInt))
    ImagePanel(scaled).displayIn("cam - points")
  }

  def checkRoundtrips(cam: CCamera, img: PixelImage[RGBA], mesh: TriangleMesh3D): Unit = {

    val t1 = 1.0e-5
    val t2 = 1.0e-3
    val camCenter = cam.cameraTransform(Point(0, 0, 0))

    val imb = img.map(_.toRGB.toRGBA).toBuffer
    mesh.pointSet.points.foreach { p3d =>
      val p2d = cam.worldToImage(p3d)

      val rpp = cam.imageToWorld(p2d)
      val intersectionPoints = mesh.operations.getIntersectionPointsOnSurface(camCenter, rpp - camCenter)
      val color = if (intersectionPoints.nonEmpty) {
        val interPoint = intersectionPoints.map { x =>
          mesh.position.onSurface(x._1, x._2)
        }.sortBy { pt =>
          (camCenter - pt).norm2
        }.head

        val mapError = (p3d - interPoint).norm
        if (mapError < t1) {
          val t = mapError / t1
          RGBA(0, 1.0 - t, t, 1)
        } else {
          val t = math.min(1.0, (mapError - t1) / t2)
          RGBA(t, 0, 1.0 - t, 1)
        }

      } else {
        RGBA.Black
      }
      for (
        dx <- -3 to 3;
        dy <- -3 to 3
      ) {
        imb(p2d.x.toInt + dx, p2d.y.toInt + dy) = color
      }
    }
    val im = imb.toImage.interpolate
    val fact = 1024.0 / imb.height
    val scaled = PixelImage((imb.width * fact).toInt, 1024, (x, y) => im((x / fact).toInt, (y / fact).toInt))
    ImagePanel(scaled).displayIn("errors - roundtripp")
  }

}
